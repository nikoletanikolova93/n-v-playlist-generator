# RidePal Playlist Generator

## 1. Description

The RidePal application is developed for users who love listening to music while they are on the road. Visitors can browse already created playlists while registered users can additionally create new ones. The generation of playlists starts with choosing the start and end points of the travel, choosing their preferred genres, as well as configuring if they'd like their playlist to exclude tracks with explicit lyrics or to have duplicate artists. Finally, users give their playlist a name and choose a picture for it.

The work on the application is split into two parts - backend API application and front-end client application. The backend API application is developed following the best practices of the REST architecture. And the frontend - following the best practices of React.

<br>

## 2. Overview

### View of the landing page

![Landing page](/screenshots/1.png)

### Most popular playlists displayed under main landing page view

![Most popular](/screenshots/2.png)

### Creating a new playlist

![Playlist location](/screenshots/6.png)

### Finalizing a new playlist

![Playlist finalize](/screenshots/12.png)

### Detailed view of a playlist

![Detailed](/screenshots/13.png)

### User browsing playlists and deleting one of theirs

![Browse/delete](/screenshots/15.png)

### User profile

![Profile](/screenshots/16.png)

### Part of the admin-only interface

![Admin](/screenshots/19.png)

### 3. Project information

- Language and version: **JavaScript ES2020**
- Platform and version: **Node 14.0+**
- Core Packages: **Express**, **React**, **ESLint**, **MariaDB**

<br>

### 4. How to setup up the project?

#### Database: To set-up the database, you are provided with the `playlistdb-populated` dump file which will create a new database populated with everything needed to run both the server and the web application.

`playlist-empty` is an SQL dump file that contains only the skeleton of the database. The provided `seed.js` file contains functions that will sequentially populate the empty database.

#### Server:

You will be exploring the server folder where is the `src` folder. This will be designated as the **root** folder, where `package.json` is placed.

You need to install all the packages/to restore all dependencies so go in the root folder and write: `npm install`  or shorthand - `npm i`.

The second step is to create a `.env` file again in the root folder. This file should contain the following information:

PORT=5555

HOST=localhost

DBPORT=3306

USER=root

PASSWORD=root

DATABASE=playlistdb

SECRET_KEY=secret_key

**Use the following setup boilerplate, keeping in mind you might need to replace the connection values with what you have setup on your machine.**

#### Client:

You will be exploring the client folder where is the `src` folder. This will be designated as the **root** folder, where `package.json` is placed.

You need to install all the packages/to restore all dependencies so go in the root folder and write: `npm install`  or shorthand - `npm i`.

The file `constants.js` in the `common` folder includes a Pixabay API key needed for generating playlist images and a Bing API key needed for the route calculation and location autosuggest. We cannot guarantee that these keys will continue working, so we advise you to replace them with your own.

Additionally, `index.html` imports a script to load the Bing Map. You may have to also replace the API key placed there.

<br>

### 5. How to run the project?

The project can be run by:

#### Server:

- `npm start` - will run the current version of the code

#### Client:

- `npm start` - will run the current version of the code

Open http://localhost:3000 to view it in the browser. The page will reload if you make edits.

- *You are all set so let's take a look what you can expect while exploring RidePal App!**

<br>

### 6. Project structure

Project's structure is designed as follows:

#### Server:

- `src/index.js` - the entry point of the project, here there are user's endpoints

- `src/auth` - contains the information for authentication; there are three files in the folder - `auth.middleware.js`, `create-token.js` and `strategy.js`.

- `src/common` - shared resources in project such as constants or enums

- `src/data` - contains the data of the project - `admins.js`, `genres.js`, `playlists.js`, `tokens.js`, `tracks.js`, `users.js`, `error-messages` - the response you will receive when trying to register a user with invalid information in the request body, `pool.js`

- `src/middleware` - contains all custom middlewares- `delete-guard.js`, `logged-user-guard.js`, `validate-body.js`

-  `src/routers` - contains information about the request from the client and the responses from the server - `admin-router.js` , `playlists-router.js`

- `src/validators` - contains schemas for validating body/query objects - `create-user-validator`

- `avatars` - stores user avatars

- `db-seed` - contains the SQL dumps and seeding functions

#### Client:

- `src/App.js` - the entry point of the project

- `src/common` - contains shared resources in the project such as constants, URLs, enums, etc

- `src/components` - contains all components in the project in subfolders:

- Admin - functionalities available only to admins:

- Admin/Playlists - admins are able to see all playlists and delete them

- Admin/Users - admins are able to delete users; deleted users are restricted from private operations in the app

- Custom Buttons

- Pages:

- BuildPlaylist - contains the components needed for creating a new playlist

- Footer

- Header - navigation through the app so you can explore around

- LandingPage - contains home view components including product and team sections

- Not Found

- Playlist - all components for displaying playlists with their names, duration, genres and rating; for searching for a playlist; detailed view with all information about a playlists - all tracks with their artists, album, genre, duration and a short audio preview.

- User - components for login/register/logout functionalities.  On a successful login a JWT token is stored in local storage. 

### 7. Designing the database

#### 7.1. The albums table

`id` - this is the primary key

`title` - the title of the album

`link` - the link of the album

`cover_image` - the cover image of the album

`genre_id` - a foreign key of genres_id

`artist_id` - a foreign key of artists_id

<br>

#### 7.2. The artists table

`id` - this is the primary key

`name` - the name of the artist

`picture` - link to the picture of the artist

`picture_small` - link to the picture of the artist in small format

`picture_medium` - link to the picture of the artist in medium format

`picture_big` - link to the picture of the artist in big format

`picture_xl` - link to the picture of the artist in XL format

<br>

#### 7.3. genres table

`id` - this is the primary key

`name` - the full name of the genre

<br>

#### 7.4. The playlists table

`id` - this is the primary key

`name` - the name of the playlist

`image` - the image of the playlist

**Images from the Pixabay API are not meant for long-term storage as their links expire after about 48 hours. The images used are for presentational purposes.**

`is_deleted` - a boolean flag, default is 0

`user_id` - a foreign key of users_id

<br>

#### 7.5. The playlists_tracks_mapping table

`id` - this is the primary key

`playlist_id` - a foreign key of playlists_id

`track_id` - a foreign key of tracks_id

<br>

#### 7.6. The tokens table

`id`- this is the primary key

`token` - the token which was created during the request/response

<br>

#### 7.7. The tracks table

`id` - this is the primary key

`title` - the title of the track

`link` - link to the track

`duration` - the duration of the track in seconds

`rank` - the ranking - using Deezer ranking

`explicit_lyrics` - a boolean flag

`preview` - link to 30 seconds preview of the track

`artist_id` - a foreign key of artists_id

`genre_id` - a foreign key of genres_id

`track_id` - a foreign key of tracks_id

<br>

#### 7.8. The users table

`id` - this is the primary key

`username` - the user name

`email` - this is the unique email of the user

`password` - this is the user's password (255)

`is_deleted` - a boolean flag, default is 0

`is_admin` - a boolean flag, default is 0

- *You are provided with a SQL Dump so you can see the design of the created database.*

There is one user with the following information: "admin" email:"admin@db.com",  password:"password".

### 8. Endpoints

- *You are provided with the following endpoints:*

#### Public:

1. POST: /users - register users - each user should have unique email, hashing is used to store passwords; response returns the new user data (without the password) or an error; work with the provided body:

body: { username: String, email: String, password: String }, validation criteria provided above

2. POST: /login - users login - JTW response (not containing password), response returns JWT or an error; work with the provided body:

body: { username: String (optional), email: String, password: String }, validation criteria provided above

3. POST: /logout - users logout - token is invalidated, response returns success message or an error

4. GET: /playlists - retrieve all playlists in the database; in addition there is search by query parameters (title or duration), due to them you can browse other's playlists and find wit similar duration; response returns all playlists or an empty array

5. GET: /playlists/:playlistId - retrieve a single playlist by its id; response returns one playlist or an error

6. DELETE: /playlist/:playlistId - delete playlist by its id, first checks weather a playlist is created by the same user, status of the playlist is updated in the database; returns the playlist' data or an error

7. GET: /preview - retrieve 6 top rated playlists in the database; response returns them

#### Private part:

1. POST: /playlist/ - create your own playlist; returns the initialized playlist or an error

body: {details: {title: String, image: String}, options: {duration: Number, allowRepeatingArtists: Boolean, allowExplicit: Boolean,

genres:{key value pairs of a supported genre's name and a value of 0, 1 or 2} }}

2. PUT: /users/avatar - upload user avatar - each user can upload or change his profile picture, he has default one; work with the provided body:

body: { key: image, value: choose from your device the file you'd like to upload }, avatars are stored in avatars folder

3. GET: /users/:userId - retrieve a single user by its id; response returns all user data or an error

4. GET: /users/:userId/history - retrieve playlist's history of a single user by its id; response returns user data or an error

#### Administration part:

1.  GET: /admin/users - retrieve all users in the application, admin is provided with search by username functionality, the response returns all users data or an error

2.  GET: /admin/users/:userId - retrieve a single user by its id, the response returns single user data or or an error

3.  DELETE: /admin/users/:userId - delete users - admin is able to delete users, deleted user is restricted from every private operation in the application, the response returns the user data or an error

4.  GET: /admin/playlists - retrieve all playlists in the application, admin is provided with search by name functionality, the response returns all playlists data or an error

5.  GET: /admin/playlists/:playlistId - retrieve a single playlist by its id, the response returns single playlist data or or an error

6.  DELETE: /admin/playlists/:playlistId - delete playlists - admin is able to delete playlists, deleted playlist not visible in all playlists but is saved in admin playlists as a history of playlists; the response returns the playlist data or an error

### 9. Authors

**Nikoleta Nikolova - A27 Telerik Academy Student**

**Vladislav Antonov - A27 Telerik Academy Student**

**Enjoy creating your personal playlists and hit the road!**

***If you have any comments, suggestions or whatever you can find our contacts in Team section and connect with us.***

**THANK YOU!**

### 10. External Services

Microsoft Bing Maps - offers similar functionality to Google maps but is free for non-commercial use

Deezer - subscription music streaming service like Spotify and Google Play

Pixabay - random music picture over generated playlists

**Pixabay image links expire after around 48 hours and should not be used for long-term storage. All images shown are for presentational purposes**

### 11. Licenses

We would like to mention that for purposes of designing our application we have used the services below:

**License**

MIT LICENSE

Copyright (c) 2021 Creative Tim.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
