import express from 'express';
import cors from 'cors';
import helmet from 'helmet';
import dotenv from 'dotenv';
import bcrypt from 'bcrypt';
import passport from 'passport';
import { checkUserId, createUser, getUserById, getUserHistory, logoutUser, updateAvatar, validateUser,
  checkIfUserIsDeleted } from './data/users.js';
import jwtStrategy from './auth/strategy.js';
import validateUserBody from './middlewares/validate-user-body.js';
import createUserValidator from './validators/create-user-validator.js';
import createToken from './auth/create-token.js';
import { authMiddleware } from './auth/auth.middleware.js';
import multer from 'multer';
import path from 'path';
import { playlistRouter } from './routers/playlists-router.js';
import genres from './data/genres.js';
import { adminRouter } from './routers/admin-router.js';
import loggedUserGuard from './middlewares/logged-user-guard.js';
import deleteGuard from './middlewares/delete-guard.js';
import { getRandomPlaylists } from './data/playlists.js';

const config = dotenv.config().parsed;

const PORT = config.PORT;

const app = express();

// Global
app.use(cors());
app.use(helmet());
app.use(express.json());

app.use('/app', express.static('public'));
app.use('/avatars', express.static('avatars'));

passport.use(jwtStrategy);
app.use(passport.initialize());

// Routers
app.use('/playlists', playlistRouter);
app.use('/admin', adminRouter);

// Public
// Get genres
app.get('/genres', async (req, res) => {
  const result = await genres.getGenres();
  res.status(200).json(result);
});

// Register User
app.post('/users', validateUserBody('user', createUserValidator), async (req, res) => {
  try {
    const user = req.body;
    user.password = await bcrypt.hash(user.password, 10);
    const result = await createUser(user.username, user.email, user.password);
    res.status(200).json(result);
  }
  catch (error) {
    res.status(400).json({ error: 'User with this email already exists!' });
  }
});

// Login user
app.post('/login', async (req, res) => {
  try {
    const user = await validateUser(req.body);
    if (user) {
      const token = createToken({
        id: user.id,
        username: user.username,
        email: user.email,
        isAdmin: !!user.is_admin,
      });

      if (await checkIfUserIsDeleted(req.body.email)) {
        return res.status(404).json('This user has been deleted!');
      }

      res.status(200).json({ token });
    } else {
      res.status(401).json({ error: 'Invalid credentials!' });
    }
  } catch (error) {
    res.status(400).json({ error: error.message });
  }
});

// Logout User
app.post('/logout', authMiddleware, async (req, res) => {
  await logoutUser(req.headers.authorization.replace('Bearer ', ''));

  res.status(200).json({ message: 'User successfully logged out!' });
});

// Upload files
const storage = multer.diskStorage({
  destination(req, file, cb) {
    cb(null, 'avatars');
  },
  filename(req, file, cb){

    const filename = Date.now() + path.extname(file.originalname);

    cb(null, filename);
  },
});

const upload = multer({ storage });

// Upload user avatar
app.put('/users/avatar', authMiddleware, upload.single('image'), async (req, res) => {
  await updateAvatar(req.file.filename, req.user.id);

  res.status(200).json({ message: 'Done!' });
});

// get user by id
app.get('/users/:userId', authMiddleware, async(req, res) => {
  const userId = req.params.userId;
  if (!(await checkUserId(userId))) {
    return res.status(404).json('User not found!');
  }
  const result = await(getUserById(userId));

  res.status(200).json(result);
});

// get user history
app.get('/users/:userId/history', authMiddleware, loggedUserGuard, deleteGuard, async (req, res) => {
  const userId = req.params.userId;
  if (!(await checkUserId(userId))) {
    return res.status(404).json('User not found!');
  }
  const result = await getUserHistory(userId);

  res.status(200).json(result);
});

// get preview of random playlists for homepage
app.get('/preview', async (req, res) => {
  const result = await getRandomPlaylists();

  res.status(200).json(result);
});

app.all('*', (req, res) => {
  res.status(404).send({ message: 'Resource not found!' });
});

// Run server
app.listen(PORT, () => console.log(`Listening on ${PORT}...`));

