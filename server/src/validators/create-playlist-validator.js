
export default {
    details: (value) => {
        if(!(typeof value === 'object')) {
            return false;
        }
        const err = [];
        if (Object.keys(value)) {
            err.push(Object.prototype.hasOwnProperty.call(value, 'title') && typeof value.title === 'string' && value.title.length > 0 && value.title.length <51);
            err.push(Object.prototype.hasOwnProperty.call(value, 'image') && typeof value.image === 'string' && value.image.length > 0 && value.image.length <201);
        } else {
            err.push(false);
        }
        return err.every(e => e === true);
    },
    options: (value) => {
        if(!(typeof value === 'object')) {
            return false;
        }
        const err = [];
        if (Object.keys(value)) {
            err.push(Object.prototype.hasOwnProperty.call(value, 'duration') && typeof value.duration === 'number' && value.duration > 0);
            err.push(Object.prototype.hasOwnProperty.call(value, 'allowRepeatingArtists') && typeof value.allowRepeatingArtists === 'boolean');
            err.push(Object.prototype.hasOwnProperty.call(value, 'allowExplicit') && typeof value.allowExplicit === 'boolean');
            
        } else {
            err.push(false);
        }
        return err.every(e => e === true);
    },
  };
  