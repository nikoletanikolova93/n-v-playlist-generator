import { EMAIL_VALIDATION, PASSWORD_VALIDATION, USERNAME_VALIDATION } from '../common/constants.js';

export default {
  username: (value) => typeof value === 'string' && value.match(USERNAME_VALIDATION),
  email: (value) => typeof value === 'string' && value.match(EMAIL_VALIDATION),
  password: (value) => typeof value === 'string' && value.match(PASSWORD_VALIDATION),
};
