import pool from './pool.js';

// This could be used to return track details to the frontend.
const getTrackById = async (trackId) => {
    const sql = `SELECT t.id, t.title, t.link, t.duration,
    t.rank, t.explicit_lyrics, t.preview,
    t.artist_id, ar.name AS artist_name,
    t.album_id, al.title AS album_title,
    t.genre_id, g.name AS genre
    FROM tracks AS t
    JOIN genres AS g ON t.genre_id = g.id
    JOIN albums AS al ON t.album_id = al.id
    JOIN artists AS ar ON t.artist_id = ar.id
    WHERE t.id = ?`;

    const result = await pool.query(sql, [trackId]);
    return result;
};

// This is to be used as a function for selecting tracks for the playlist generation
// algorithm and nothing else. Do not expose as the logic by design is not safe from
// SQL injections. 
const getTrackWithParams = async (genre, duration = null, artistBlacklist = [], allowExplicit) => {
    const sql = `SELECT t.id, t.title, t.link, t.duration,
    t.rank, t.explicit_lyrics, t.preview,
    t.artist_id, ar.name AS artist_name,
    t.album_id, al.title AS album_title,
    t.genre_id, g.name AS genre
    FROM tracks AS t
    JOIN genres AS g ON t.genre_id = g.id
    JOIN albums AS al ON t.album_id = al.id
    JOIN artists AS ar ON t.artist_id = ar.id
    WHERE g.name = "${genre}"
    ${!allowExplicit ? `AND t.explicit_lyrics = 0` : ''}
    ${artistBlacklist.map(a => `AND NOT ar.name = "${a}"`).join(' ')}
    ${ duration ? `AND ROUND(duration, -1) = ${Math.round(duration /10) * 10}` :''}
    ORDER BY RAND()
    LIMIT 1`;

    const result = await pool.query(sql);
    return result[0];
};

export default {
    getTrackById,
    getTrackWithParams,
};