import pool from './pool.js';
import tracks from './tracks.js';

// Use getAllPlaylists to return the overview of the playlists and use their
// id to load details with getPlaylistById?
export const getAllPlaylists = async (name, duration) => {
    let allPlaylists = null;
    if (name) {
        allPlaylists = await pool.query(`
        SELECT p.id, p.name, p.image, p.is_deleted, p.user_id, u.username,
        SUM(t.duration) AS duration, ROUND(SUM(t.rank)/COUNT(t.rank), 0) AS rank
        FROM playlists_tracks_mapping AS ptm
        JOIN playlists AS p ON p.id = ptm.playlist_id
        JOIN tracks AS t ON ptm.track_id = t.id
        JOIN users AS u ON p.user_id = u.id
        WHERE p.is_deleted = 0
        GROUP BY p.id
        HAVING p.name
        LIKE '%${pool.escape(name).replace(/'/g, '')}%'
        ORDER BY rank DESC;
        `);

        // searching by the exact duration - will be helpful if we have a functionality - view playlists with similar duration
        // if duration is 1797 the search should be exactly 1797, if it is only 1 or 8 or whatever it won't work
        // if need changes wrap the search value in %%
    } else if (duration) {
        allPlaylists = await pool.query(`
        SELECT p.id, p.name, p.image, p.is_deleted, p.user_id, u.username,
        SUM(t.duration) AS duration, ROUND(SUM(t.rank)/COUNT(t.rank), 0) AS rank
        FROM playlists_tracks_mapping AS ptm
        JOIN playlists AS p ON p.id = ptm.playlist_id
        JOIN tracks AS t ON ptm.track_id = t.id
        JOIN users AS u ON p.user_id = u.id
        WHERE p.is_deleted = 0
        GROUP BY p.id
        HAVING ROUND(SUM(t.duration),-2)
        LIKE ROUND('${pool.escape(duration).replace(/'/g, '')}',-2)
        ORDER BY rank DESC;
        `);
    } else {

        allPlaylists = await pool.query(`
    SELECT p.id, p.name, p.image, p.is_deleted, p.user_id, u.username,
    SUM(t.duration) AS duration, ROUND(SUM(t.rank)/COUNT(t.rank), 0) AS rank
    FROM playlists_tracks_mapping AS ptm
    JOIN playlists AS p ON p.id = ptm.playlist_id
    JOIN tracks AS t ON ptm.track_id = t.id
    JOIN users AS u ON p.user_id = u.id
    WHERE p.is_deleted = 0
    GROUP BY p.id
    ORDER BY rank DESC;
    `);
    }

    const playlistIds = allPlaylists.map(p => p.id);
    const playlistGenres = await Promise.all(playlistIds.map(async p => await getPlaylistGenres(p)));
    const playlistGenresArray = playlistGenres.map(p => p.map(e => e.genre));
    allPlaylists.forEach(p => p.genre = playlistGenresArray[allPlaylists.indexOf(p)]);

    return allPlaylists;
};

// experiment with ways to return the playlist and track data, arrays? objects?
export const getPlaylistById = async (playlistId) => {
    const sql = `SELECT p.id, p.name, p.image, p.is_deleted, p.user_id, u.username,
    SUM(t.duration) AS duration, ROUND(SUM(t.rank)/COUNT(t.rank), 0) AS rank
    FROM playlists_tracks_mapping AS ptm
    JOIN playlists AS p ON p.id = ptm.playlist_id
    JOIN tracks AS t ON ptm.track_id = t.id
    JOIN users AS u ON p.user_id = u.id
    WHERE ptm.playlist_id = ?`;
    const playlist = await pool.query(sql, [playlistId]);

    const tracklist = await getPlaylistTracks(playlist[0].id);

    const genres = await getPlaylistGenres(playlistId);
    const genreArr = genres.map(e => e.genre);
    playlist[0].genre = genreArr;

    return ({ playlist, tracks: tracklist });
};

// to be used by the algorithm for creating a new playlist
export const createPlaylist = async ({ title, image }, userId, tracklist = []) => {
    const sql1 = `INSERT INTO playlists (name, image, user_id, is_deleted)
    VALUES (?, ?, ?, 0)`;

    const newPlaylist = await pool.query(sql1, [title, image, userId]);

    const sql2 = `INSERT INTO playlists_tracks_mapping (playlist_id, track_id)
    VALUES (?, ?)`;

    tracklist.forEach(async (t) => await pool.query(sql2, [newPlaylist.insertId, t.id]));

    const result = await getPlaylistById(newPlaylist.insertId);

    return result;
};

// internal function that returns only the tracklist of the given playlist 
const getPlaylistTracks = async (playlistId) => {
    const sql = `SELECT t.id, t.title, t.link, t.duration,
    t.rank, t.explicit_lyrics, t.preview,
    t.artist_id, ar.name AS artist_name,
    t.album_id, al.title AS album_title,
    t.genre_id, g.name AS genre
    FROM playlists_tracks_mapping AS ptm
    JOIN tracks AS t ON ptm.track_id = t.id
    JOIN genres AS g ON t.genre_id = g.id
    JOIN albums AS al ON t.album_id = al.id
    JOIN artists AS ar ON t.artist_id = ar.id  
    WHERE ptm.playlist_id = ?
    ORDER BY t.id`;

    const result = await pool.query(sql, [playlistId]);

    return result;
};

const generatePlaylist = async ({ duration, genres, allowRepeatingArtists = false, allowExplicit = true }) => {
    // initialize list of required genres for playlist
    let reqGenres = [];
    if (Object.values(genres).every(g => g === 2)) {
        for (let genre in genres) {
            genres[genre] = 1;
        }
    }
    // populate array of required genres
    for (let g in genres) {
        reqGenres = reqGenres.concat((Array(+genres[g]).fill(g)));
        // [jazz, jazz, rock, dance]
    }
    //initialize counter for which genre to look for
    let counter = reqGenres.length;
    let currentGenre = reqGenres[counter % reqGenres.length];
    //initialize other variables
    let remainingTime = duration;
    let currentTrack = null;
    const artists = [];
    const trackList = [];

    //main loop for finding tracks
    while (remainingTime > 0) {
        currentTrack = await tracks.getTrackWithParams(currentGenre, null, artists, allowExplicit);
        if ((remainingTime - currentTrack.duration) < -30) {
            break;
        }
        trackList.push(currentTrack);
        remainingTime -= currentTrack.duration;
        if (!allowRepeatingArtists) {
            artists.push(currentTrack.artist_name);
        }
        counter++;
        currentGenre = reqGenres[counter % reqGenres.length];
    }
    // look for a final track to top off tracklist
    currentTrack = await tracks.getTrackWithParams(currentGenre, remainingTime, artists, allowExplicit);
    if (currentTrack) {
        trackList.push(currentTrack);
    }
    return trackList;
};

export const initializePlaylist = async (userId, playlistDetails, playlistOptions) => {
    try {
        const generatedPlaylist = await generatePlaylist(playlistOptions);
        const newPlaylist = await createPlaylist(playlistDetails, userId, generatedPlaylist);
        return newPlaylist;
    } catch (error) {
        return false;
    }

};

export const checkPlaylistId = async (playlistId) => {
    const result = await pool.query(`SELECT COUNT(*) as count FROM playlists WHERE playlists.id = ?;`, [playlistId]);
    return result[0].count;
};

// const details = {
//     title: 'Testing testing',
//     image: 'okay.jpg',
// };

//     userId: 1,
// //use genre IDs?

// const options = {
//     duration: 1800,
//     allowExplicit: true,
//     allowRepeatingArtists: false,
//     genres: {
//         jazz:1,
//         rock:1,
//         dance:1,
//         'r&b':1,
//         'latin music':0,
//     },
// };

//initializePlaylist(details,options);


// example with async wrapper
// NOTE: this uses an already created user with id: 1, make sure you have one

// const testFunc = async () => {
//     const test = await createPlaylist('This should work', 'link disc', 1, [{id: 1017670902}, {id: 100479032}, {id: 5817325}]);
//     console.log(test);
// };
// testFunc();

export const getPlaylistGenres = async (playlistId) => {

    return await pool.query(`
    SELECT Distinct g.name AS genre
    FROM playlists_tracks_mapping AS ptm
    JOIN tracks AS t ON ptm.track_id = t.id
    JOIN genres AS g ON t.genre_id = g.id
    WHERE ptm.playlist_id = ?`, [playlistId]);
};

export const checkPlaylistOwner = async (playlistId, userId) => {
    const sql = `SELECT COUNT(*) AS count
    FROM playlists
    WHERE id = ? AND user_id = ?`;

    const result = await pool.query(sql, [playlistId, userId]);
    return result[0].count;
};

// takes the top rated playlists
// if want to have less or more playlists on the homepage change LIMIT
export const getRandomPlaylists = async () => {
    const randomPlaylists = await pool.query(`
    SELECT p.id, p.name, p.image, p.is_deleted, p.user_id, u.username,
    SUM(t.duration) AS duration, ROUND(SUM(t.rank)/COUNT(t.rank), 0) AS rank
    FROM playlists_tracks_mapping AS ptm
    JOIN playlists AS p ON p.id = ptm.playlist_id
    JOIN tracks AS t ON ptm.track_id = t.id
    JOIN users AS u ON p.user_id = u.id
    WHERE p.is_deleted = 0
    GROUP BY p.id
    ORDER BY rank DESC
    LIMIT 6
    `);

    const playlistIds = randomPlaylists.map(p => p.id);
    const playlistGenres = await Promise.all(playlistIds.map(async p => await getPlaylistGenres(p)));
    const playlistGenresArray = playlistGenres.map(p => p.map(e => e.genre));
    randomPlaylists.forEach(p => p.genre = playlistGenresArray[randomPlaylists.indexOf(p)]);

    return randomPlaylists;
};

export default {
    getAllPlaylists,
    getPlaylistById,
    createPlaylist,
    initializePlaylist,
    checkPlaylistId,
};
