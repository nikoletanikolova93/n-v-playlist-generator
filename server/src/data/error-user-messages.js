export default {
  user: {
    username: 'Expected username should be between 3 and 45 symbols and could contain letters, numbers and _',
    email: 'Expected email should be in the format: example@example.com',
    password: 'Expected password should be between 8 and 15 symbols and must have at least one lowercase or uppercase letter and a number',
  },
  playlist: {
    details: 'Expected properties of: title between 1 and 50 symbols and image between 1 and 200 symbols',
    options: 'Expected properties of: duration as positive number, allowRepeatingArtists as boolean and allowExplicit as boolean',
  },
};
