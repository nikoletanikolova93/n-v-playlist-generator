import pool from './pool.js';
import bcrypt from 'bcrypt';
import { getPlaylistGenres } from './playlists.js';

export const getAllUsers = async () => {
  const sql = `SELECT u.id, u.username, u.email, u.is_deleted, u.image, u.is_admin FROM users AS u;`;

  const result = await pool.query(sql);
  return result;
};

export const getUserById = async (userId) => {
  const sql = `SELECT u.id, u.username, u.email, u.image, u.is_deleted, u.is_admin FROM users AS u WHERE u.id = ?;`;

  const result = await pool.query(sql, [userId]);
  return result[0];
};

export const getUserByEmail = async (userEmail) => {
  const sql = `SELECT * FROM users AS u WHERE u.email = ?;`;

  const result = await pool.query(sql, [userEmail]);
  return result(0);
};

export const deleteUser = async (userId) => {
  const sql = `UPDATE users AS u SET u.is_deleted = 1 WHERE u.id = ?;`;

  const result = await pool.query(sql, [userId]);
  return result[0];
};

export const createUser = async (username, email, password) => {
  const sql = `INSERT INTO users (username, email, password) 
    VALUES (?, ?, ?);`;
    const result = await pool.query(sql, [username, email, password]);

    const sql2 = `SELECT id, username, email FROM users AS u WHERE u.id = ?`;
    const user = await pool.query(sql2, [result.insertId]);
    return user[0];
};

export const validateUser = async ({ email, password }) => {
  const userData = await pool.query(`
  SELECT * FROM users AS u WHERE u.email = ?;`, [email]);

  if (userData.length === 0) {
    throw new Error('The email does not exist!');
  }

  if (await bcrypt.compare(password, userData[0].password)) {
    return userData[0];
  }

  return null;
};

export const logoutUser = async (token) => {
  return await pool.query(`INSERT INTO tokens (token) VALUES (?);`, [token]);
};

export const updateAvatar = async (filename, userId) => {
  const sql = `UPDATE users AS u SET u.image = ? WHERE u.id = ?`;

  const result = await pool.query(sql, [filename, userId]);

  return {
    success: result.affectedRows === 1,
    response: {},
  };
};

export const checkUserId = async (userId) => {
  const result = await pool.query(`SELECT COUNT(*) as count FROM users WHERE users.id = ?;`, [userId]);
  return result[0].count;
};

export const checkIfUserIsDeleted = async (email) => {
  const sql = `SELECT u.is_deleted FROM users AS u WHERE u.email = ?;`;

  const result = await pool.query(sql, [email]);
  
  if (result[0].is_deleted) {
    return true;
  }
  return false;
};

export const getUserHistory = async (userId) => {
  let history =  await pool.query(`
    SELECT p.id, p.name AS name, p.image AS image, p.is_deleted,
    SUM(t.duration) AS duration, 
    ROUND(SUM(t.rank)/COUNT(t.rank), 0) AS rank
    FROM playlists_tracks_mapping AS ptm
    JOIN playlists AS p ON ptm.playlist_id = p.id
    JOIN tracks AS t ON ptm.track_id = t.id
    WHERE p.user_id = ? AND p.is_deleted = 0
    GROUP BY p.name`, [userId]);

  const playlistIds = history.map(p => p.id);
  const playlistGenres = await Promise.all(playlistIds.map(async p =>  await getPlaylistGenres(p)));
  const playlistGenresArray = playlistGenres.map(p => p.map(e => e.genre));
  history.forEach(p => p.genre = playlistGenresArray[history.indexOf(p)]);

  return history;
};



