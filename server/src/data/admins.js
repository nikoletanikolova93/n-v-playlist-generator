import { getPlaylistGenres } from './playlists.js';
import pool from './pool.js';

export const checkIfUserIsAdmin = async (userId) => {
  const sql = `SELECT COUNT(*) as count FROM users as u WHERE u.is_admin = 1 AND u.id = ?;`;

  const result = await pool.query(sql, [userId]);
  if (!result[0].count) {
    return false;
  }
  return true;
};

export const getAllUsersForAdmins = async (name) => {
  let allUsers = null;
  if (name) {
    allUsers = await pool.query(`
      SELECT u.id, u.username, u.email, u.image, u.is_deleted, u.is_admin FROM users AS u
      HAVING u.username
      LIKE '%${pool.escape(name).replace(/'/g, '')}%'`);
  } 
  else {
    allUsers = await pool.query(
      `SELECT u.id, u.username, u.email, u.image, u.is_deleted, u.is_admin FROM users AS u;`);
  }

  return allUsers;
};

export const getUserByIdForAdmins = async (userId) => {
  const sql = `SELECT u.id, u.username, u.email, u.image, u.is_deleted, u.is_admin FROM users AS u
  WHERE u.id = ?;`;

  const result =  await pool.query(sql, [userId]);
  return result[0];
};

export const deleteUser = async (userId) => {
  const sql = `UPDATE users SET is_deleted = 1 WHERE id = ?`;

  const result = await pool.query(sql, [userId]);
  return result[0];
};

export const deletePlaylist = async (playlistId) => {
  const sql = `UPDATE playlists SET is_deleted = 1 WHERE id = ?`;

  const result = await pool.query(sql, [playlistId]);
  return result[0];
};

export const getAllPlaylistsForAdmins = async (name) => {
  let allPlaylists = null;
  if (name) {
    allPlaylists = await pool.query(`
    SELECT p.id, p.name, p.image, p.is_deleted, p.user_id, u.username,
    SUM(t.duration) AS duration, ROUND(SUM(t.rank)/COUNT(t.rank), 0) AS rank
    FROM playlists_tracks_mapping AS ptm
    JOIN playlists AS p ON p.id = ptm.playlist_id
    JOIN tracks AS t ON ptm.track_id = t.id
    JOIN users AS u ON p.user_id = u.id
    GROUP BY p.id
    HAVING p.name
    LIKE '%${pool.escape(name).replace(/'/g, '')}%';
    `);
  }
  else {
    allPlaylists = await pool.query(`
    SELECT p.id, p.name, p.image, p.is_deleted, p.user_id, u.username,
    SUM(t.duration) AS duration, ROUND(SUM(t.rank)/COUNT(t.rank), 0) AS rank
    FROM playlists_tracks_mapping AS ptm
    JOIN playlists AS p ON p.id = ptm.playlist_id
    JOIN tracks AS t ON ptm.track_id = t.id
    JOIN users AS u ON p.user_id = u.id
    GROUP BY p.id
    `);
  }

  const playlistIds = allPlaylists.map(p => p.id);
  const playlistGenres = await Promise.all(playlistIds.map( async p =>  await getPlaylistGenres(p)));
  const playlistGenresArray = playlistGenres.map(p => p.map(e => e.genre));
  allPlaylists.forEach(p => p.genre=playlistGenresArray[allPlaylists.indexOf(p)]);

  return allPlaylists;
};