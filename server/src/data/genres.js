import pool from './pool.js';

const getGenres = async () => {
    const sql = `SELECT * FROM genres`;

    const result = await pool.query(sql);

    return result;
};

export default {
    getGenres,
};
