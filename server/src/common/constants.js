export const DEEZER_API_KEY = '4dd430ea7036d103cb767ea9178d716b';

//regex validation - username should be between 3 and 45 symbols and could contain letters, numbers and _
export const USERNAME_VALIDATION = /^[a-zA-z0-9]{3,45}$/;

// regex validation - email should be in the format: example@example.com
export const EMAIL_VALIDATION = /^([\w.-]+)@([\w-]+)((\.(\w){2,3})+)$/;

// regex validation - password should be between 8 and 15 symbols and must have at least one lowercase or uppercase letter and a number
export const PASSWORD_VALIDATION = /(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{8,15})$/;