import { getUserById } from '../data/users.js';

export default async (req, res, next) => {
  const userId = await getUserById(req.user.id);

  if (userId.is_deleted === 1) {
    return res.status(403).json({ error: 'User is deleted!' });
  }

  await next();
};