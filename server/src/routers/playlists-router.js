import express from 'express';
import { authMiddleware } from '../auth/auth.middleware.js';
import { deletePlaylist } from '../data/admins.js';
import { getAllPlaylists, getPlaylistById, checkPlaylistId, initializePlaylist, checkPlaylistOwner } from '../data/playlists.js';
import deleteGuard from '../middlewares/delete-guard.js';
import validateUserBody from '../middlewares/validate-user-body.js';
import createPlaylistValidator from '../validators/create-playlist-validator.js';

export const playlistRouter = express.Router();

// searching all playlists by name or duration
playlistRouter.get('/', async (req, res) => {
  if (!req.query) {
    res.status(200).json(await getAllPlaylists(req.query.name, req.query.duration));
    return;
  }
  
  const result = await getAllPlaylists(req.query.name, req.query.duration);

  return res.status(200).json(result);
});

// getting all playlists
playlistRouter.get('/', async (req, res) => {
  const result = await getAllPlaylists();

  return res.status(200).json(result);
});

// getting a playlist by id
playlistRouter.get('/:playlistId', async (req, res) => {
  if (!(await checkPlaylistId(req.params.playlistId))) {
    return res.status(404).json('This playlist does not exist!');
  }
  const result = await getPlaylistById(req.params.playlistId);
  return res.status(200).json(result);
});

// create playlist
playlistRouter.post('/', authMiddleware, deleteGuard, validateUserBody('playlist', createPlaylistValidator), async (req, res) => {
  const userId = req.user.id;
  const result = await initializePlaylist(userId, req.body.details, req.body.options);
  if(result) {
    return res.status(200).json(result);
  }
  return res.status(406).json(`Can't generate playlist with given details`);
});

// deleting playlist by id
playlistRouter.delete('/:playlistId', authMiddleware, deleteGuard, async (req, res) => {
  const userId = req.user.id;
  if (!(await checkPlaylistId(req.params.playlistId))) {
    return res.status(404).json('This playlist does not exist!');
  }
  if (!(await checkPlaylistOwner(req.params.playlistId, userId))) {
    return res.status(401).json('You are not creator of the playlist.');
  }
  
  await deletePlaylist(req.params.playlistId);
  const result = await getPlaylistById(req.params.playlistId);
  return res.status(200).json(result);
});
