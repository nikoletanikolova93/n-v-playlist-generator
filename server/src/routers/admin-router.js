import express from 'express';
import { authMiddleware } from '../auth/auth.middleware.js';
import { checkIfUserIsAdmin, deletePlaylist, deleteUser, getAllPlaylistsForAdmins, getAllUsersForAdmins, getUserByIdForAdmins } from '../data/admins.js';
import { checkPlaylistId, getPlaylistById } from '../data/playlists.js';
import { checkUserId } from '../data/users.js';

export const adminRouter = express.Router();
adminRouter.use(authMiddleware);

// controller for searching all users by name
adminRouter.get('/users', async(req, res) => {
  const adminId = req.user.id;

  if (!(await checkIfUserIsAdmin(adminId))) {
    return res.status(401).json('You are not authorized. Accessible only for admins.');
  }
  if (!req.query) {
    res.status(200).json(await getAllUsersForAdmins(req.query.name));
    return;
  }

  const result = await getAllUsersForAdmins(req.query.name);
  res.status(200).json(result);
});

// controller for getting all users
adminRouter.get('/users', async(req, res) => {
  const adminId = req.user.id;

  if (!(await checkIfUserIsAdmin(adminId))) {
    return res.status(401).json('You are not authorized. Accessible only for admins.');
  }

  const result = await getAllUsersForAdmins();
  res.status(200).json(result);
});

// controller for getting user by id
adminRouter.get('/users/:userId', async(req, res) => {
  const userId = req.params.userId;
  const adminId = req.user.id;

  if (!(await checkIfUserIsAdmin(adminId))) {
    return res.status(401).json('You are not authorized. Accessible only for admins.');
  }
  if (!(await checkUserId(userId))) {
    return res.status(404).json('User not found!');
  }

  const result = await getUserByIdForAdmins(userId);
  res.status(200).json(result);
});

// controller for deleting user
adminRouter.delete('/users/:userId', async(req, res) => {
  const userId = req.params.userId;
  const adminId = req.user.id;

  if (!(await checkIfUserIsAdmin(adminId))) {
    return res.status(401).json('You are not authorized. Accessible only for admins.');
  }
  if (!(await checkUserId(userId))) {
    return res.status(404).json('User not found!');
  }

  await deleteUser(userId);
  const result = await getUserByIdForAdmins(userId);
  res.status(200).json(result);
});

// controller for searching all playlists by name
adminRouter.get('/playlists', async(req, res) => {
  const adminId = req.user.id;

  if (!(await checkIfUserIsAdmin(adminId))) {
    return res.status(401).json('You are not authorized. Accessible only for admins.');
  }
  if (!req.query) {
    res.status(200).json(await getAllPlaylistsForAdmins(req.query.name));
    return;
  }

  const result = await getAllPlaylistsForAdmins(req.query.name);
  res.status(200).json(result);
});

// controller for getting all playlists
adminRouter.get('/playlists', async(req, res) => {
  const adminId = req.user.id;

  if (!(await checkIfUserIsAdmin(adminId))) {
    return res.status(401).json('You are not authorized. Accessible only for admins.');
  }

  const result = await getAllPlaylistsForAdmins();
  res.status(200).json(result);
});

// controller for getting playlist by id
adminRouter.get('/playlists/:playlistId', async(req, res) => {
  const playlistId = req.params.playlistId;
  const adminId = req.user.id;

  if (!(await checkIfUserIsAdmin(adminId))) {
    return res.status(401).json('You are not authorized. Accessible only for admins.');
  }
  if (!(await checkPlaylistId(playlistId))) {
    return res.status(404).json('Playlist not found!');
  }

  const result = await getPlaylistById(playlistId);
  res.status(200).json(result);
});

// controller for deleting playlist
adminRouter.delete('/playlists/:playlistId', async(req, res) => {
  const playlistId = req.params.playlistId;
  const adminId = req.user.id;

  if (!(await checkIfUserIsAdmin(adminId))) {
    return res.status(401).json('You are not authorized. Accessible only for admins.');
  }
  if (!(await checkPlaylistId(playlistId))) {
    return res.status(404).json('Playlist not found!');
  }

  await deletePlaylist(playlistId);
  const result = await getPlaylistById(playlistId);
  res.status(200).json(result);
});