import fetch from 'node-fetch';
import pool from '../src/data/pool.js';

const URL = 'https://api.deezer.com';
//Use the deezer API to find the IDs of the genres you want to seed
const genreIDs = [113, 152, 165, 129, 197];

const seedGenres = (genres) => {

    const fetchGenre = (genreId) => {
        fetch(`${URL}/genre/${genreId}`)
            .then(res => res.json())
            .then(res => insertGenre(res.id, res.name))
            .then(() => console.log('Genre successfully inserted'));
    };

    const insertGenre = async (id, name) => {
        const sql = `INSERT INTO genres (id, name) VALUES (?, ?)`;
        await pool.query(sql, [id, name]);
    };

    genres.forEach(g => fetchGenre(g));
};


const seedArtists = (genres) => {

    const fetchArtists = (genreId) => {
        fetch(`${URL}/genre/${genreId}/artists`)
            .then(res => res.json())
            .then(res => res.data.forEach(artist => {
                console.log(`Inserting ${artist.name}`);
                insertArtist(artist);
            }));
    };

    const insertArtist = async (artist) => {
        const sql = `INSERT INTO artists (id, name, picture, picture_small, picture_medium, picture_big, picture_xl)
        VALUES (?,?,?,?,?,?,?)`;

        await pool.query(sql, [artist.id, artist.name, artist.picture,
        artist.picture_small, artist.picture_medium, artist.picture_big, artist.picture_xl]);
    };

    genres.forEach(genre => {
        console.log(`Seeding ${genre} artists`);
        fetchArtists(genre);
    });
};


const seedAlbums = async (genres) => {

    const getArtists = async () => {
        const sql = `SELECT id FROM artists`;

        const result = await pool.query(sql);
        return result;
    };

    const fetchAlbums = async (artistId, albumNum) => {
        const res = await fetch(`${URL}/artist/${artistId}/albums`);
        const parsed = await res.json();

        const result = await parsed.data.filter(album => genres.includes(album.genre_id));
        if (result.length > albumNum) {
            return result.slice(0, albumNum);
        }
        if (result.length === 0) {
            console.log('No albums found with the selected genres');
        }
        return result;
    };

    const insertAlbums = async (album, artistId) => {
        const sql = `INSERT INTO albums (id, title, link, cover_image, genre_id, artist_id)
        VALUES (?,?,?,?,?,?)`;

        try {
            await pool.query(sql, [album.id, album.title, album.link,
            album.cover, album.genre_id, artistId]);
        } catch (error) {
            console.log('Skipping duplicate album');
        }
    };

    const delayedFetch = async (ent) => {

        const sleep = (ms) => {
            return new Promise(resolve => setTimeout(resolve, ms));
        };

        for (let i = 0; i < ent.length; i++) {
            let currentAlbums = null;
            if (i === 0) {
                console.log('Begin album seed. This will take a while');
            }
            if (i % 10 === 0) {
                console.log('Cooling down');
                await sleep(4000);
                currentAlbums = await fetchAlbums(ent[i].id, 5);
                currentAlbums.forEach(a => insertAlbums(a, ent[i].id));
            }
            else {
                sleep(2000);
                currentAlbums = await fetchAlbums(ent[i].id, 5);
                currentAlbums.forEach(a => insertAlbums(a, ent[i].id));
            }
            if (i === ent.length - 1) {
                console.log('Seed finished');
            }
        }
    };

    const artistList = await getArtists();
    delayedFetch(artistList);
};


const seedTracks = async () => {

    const getAlbums = async () => {
        const sql = `SELECT id as album_id, artist_id, genre_id FROM albums`;

        const result = await pool.query(sql);
        return result;
    };

    const fetchTracks = async (albumId, trackNum) => {
        const res = await fetch(`${URL}/album/${albumId}/tracks`);
        const parsed = await res.json();

        const result = await parsed.data;

        if (result.length > trackNum) {
            return result.slice(0, trackNum);
        }
        return result;
    };

    const insertTracks = async (track, artistId, albumId, genreId) => {
        const sql = `INSERT INTO tracks (id, title, link, duration, rank,
            explicit_lyrics, preview, artist_id, album_id, genre_id)
        VALUES (?,?,?,?,?,?,?,?,?,?)`;

        try {
            await pool.query(sql, [track.id, track.title, track.link, track.duration,track.rank,
                track.explicit_content_lyrics ,track.preview, artistId, albumId, genreId]);
        } catch (error) {
            console.log('Skipping track; title too long.');
        }
    };

    const delayedFetch = async (ent) => {

        const sleep = (ms) => {
            return new Promise(resolve => setTimeout(resolve, ms));
        };

        for (let i = 0; i < ent.length; i++) {
            let currentTracks = null;
            if (i === 0) {
                console.log('Begin track seed. This will take a while');
            }
            if (i % 10 === 0) {
                console.log('Cooling down');
                await sleep(4000);
                currentTracks = await fetchTracks(ent[i].album_id, 5);
                currentTracks.forEach(t => insertTracks(t, ent[i].artist_id, ent[i].album_id, ent[i].genre_id));
            }
            else {
                sleep(2000);
                currentTracks = await fetchTracks(ent[i].album_id, 5);
                currentTracks.forEach(t => insertTracks(t, ent[i].artist_id, ent[i].album_id, ent[i].genre_id));
            }
            if (i === ent.length - 1) {
                console.log('Seed finished');
            }
        }
    };

    const albumList = await getAlbums();
    delayedFetch(albumList);
};


seedGenres();
seedArtists(genreIDs);
seedAlbums(genreIDs);
seedTracks();
