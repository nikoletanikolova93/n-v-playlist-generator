import { createContext } from 'react';
import jwtDecode from 'jwt-decode';

const AuthContext = createContext({
  isLoggedIn: false,
  user: null,
  isAdmin: false,
  setAuthState: () => {},
});

export const getToken = () => localStorage.getItem('token') || '';

export const getUser = () => {
  try {
    return jwtDecode(getToken());
  } catch (error) {
    return null;
  }
};

export default AuthContext;
