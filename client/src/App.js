import React, { useState } from 'react';
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';
import NotFound from './components/Pages/NotFound/NotFound';
import AuthContext, { getToken, getUser } from './providers/authContext';
import { Register } from './components/User/Register/Register';
import { toast } from 'react-toastify';
import Login from './components/User/Login/Login';
import UserProfile from './components/User/UserProfile/UserProfile';
import AllPlaylists from './components/Playlist/AllPlaylists';
import SinglePlaylist from './components/Playlist/SinglePlaylist';
import Header from './components/Pages/Header/Header';
import BuildPlaylist from './components/Pages/BuildPlaylist/BuildPlaylist';
import LandingPage from './components/Pages/LandingPage/LandingPage';
import GuardedRoute from './providers/GuardedRoute';
import "./assets/scss/material-kit-react.scss";
import AllUsers from './components/Admin/Users/AllUsers';
import Playlists from './components/Admin/Playlists/Playlists';
import Footer from './components/Pages/Footer/Footer';

toast.configure();

const App = () => {

  const [authValue, setAuthState] = useState({
    user: getUser(),
    isLoggedIn: Boolean(getToken()),
    isAdmin: getUser() ? getUser().isAdmin : false,
  });

  return (
      <BrowserRouter>
        <div className="main-container">
          <AuthContext.Provider value={{ ...authValue, setAuthState }}>
            <Header />
            <div className="App">
              <Switch>
                <Redirect path="/" exact to="/home" />
                <Route path="/home" component={LandingPage} />
                <Route exact path='/playlists' isLoggedIn={!authValue.isLoggedIn} component={AllPlaylists} />
                <Route exact path='/playlists/:playlistId' isLoggedIn={!authValue.isLoggedIn} component={SinglePlaylist} />
                <Route path="/register" isLoggedIn={!authValue.isLoggedIn} component={Register} />
                <GuardedRoute path="/login" isLoggedIn={!authValue.isLoggedIn} component={Login} />
                <GuardedRoute exact path="/users/profile" isLoggedIn={authValue.isLoggedIn} component={UserProfile} />
                <GuardedRoute exact path="/build-a-playlist" isLoggedIn={authValue.isLoggedIn} component={BuildPlaylist} />
                <GuardedRoute exact path="/admin/users" isLoggedIn={authValue.isLoggedIn && authValue.isAdmin} component={AllUsers} />
                <GuardedRoute exact path="/admin/playlists" isLoggedIn={authValue.isLoggedIn && authValue.isAdmin} component={Playlists} />
                <Route path="*" component={NotFound} />
              </Switch>
            </div>
          </AuthContext.Provider>
          <Footer />
        </div>
      </BrowserRouter>
  );
};

export default App;
