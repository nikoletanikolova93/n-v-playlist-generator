import React, { useContext, useEffect, useState } from 'react';
import AuthContext, { getToken } from '../../../providers/authContext';
import { BASE_URL } from '../../../common/constants';
import { Card, Container, InputBase, Typography } from '@material-ui/core';
import DeleteUser from './DeleteUser';
import { fade, makeStyles } from '@material-ui/core/styles';
import SearchIcon from '@material-ui/icons/Search';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  search: {
    position: 'absolute',
    marginTop: '-30px',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(3),
      width: 'auto',
    },
  },
  searchIcon: {
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'right',
    pointerEvents: 'none',
    display: 'inline',
    alignItems: 'center',
    justifyContent: 'center',
  },
}));

const AllUsers = (props) => {
  const classes = useStyles();
  const [allUsers, setAllUsers] = useState([]);
  // eslint-disable-next-line
  const [error, setError] = useState(null);
  const { isLoggedIn , isAdmin } = useContext(AuthContext);
  const history = props.history;
  const queryParams = window.location.search;
  const [search, setSearch] = useState('');

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const fetchGetAllUsers = () => {
    fetch(`${BASE_URL}/admin/users/${queryParams}`, {
      headers: {
        Authorization: `Bearer ${getToken()}`,
      },
  })
    .then(r => r.json())
    .then(r => {
      if (Array.isArray(r)) {
        setAllUsers(r);
      } else {
        setError('There are no users!');
      }
    })
  };

  useEffect(() => {
    fetchGetAllUsers();
  }, [history, isLoggedIn, isAdmin, fetchGetAllUsers]);

  const deleteUser = (userId) => {
    fetch(`${BASE_URL}/admin/users/${userId}`, {
      method: 'DELETE',
      headers: {
        Authorization: `Bearer ${getToken()}`
        }
      })
    .then(r => r.json())
    .then(() => fetchGetAllUsers())
  };
  
  if (allUsers) {
    return (
      <div className={classes.root}>
        <Container maxWidth="md">
          <br></br>
          <Typography variant="h5" style={{ textAlign: "center" }} >
            {!allUsers.length ? 'No users found' : 'All users'}
            </Typography>
            <Typography style={{ textAlign: "right" }} >

          <div className={classes.search}>
            <div className={classes.searchIcon}>
              <SearchIcon />
            </div>
          <InputBase
              placeholder="Search..."
              classes={{
                root: classes.inputRoot,
                input: classes.inputInput,
              }}
              inputProps={{ 'aria-label': 'search' }}
              type="text" className="mr-sm-2" 
              onSubmit={e => {e.preventDefault()}} 
              onChange={e => setSearch(e.target.value)}
              onKeyUp={() => history.push(`/admin/users?name=${search}`)}
              />
          </div>
              </Typography>
          <br></br>
          <Card>
            {allUsers.map(u => 
              (<DeleteUser 
                user={u} 
                userId={u.id} 
                fetchGetAllUsers={fetchGetAllUsers} 
                deleteUser={deleteUser} 
                key={u.id} />)
          )}
          </Card>
        </Container>
      </div>
    );
  }

};

export default AllUsers;
