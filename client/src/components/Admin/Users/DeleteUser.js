import React from 'react';
import { Button, Paper, Table, TableBody, TableCell, TableContainer, TableRow } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import DeleteIcon from '@material-ui/icons/Delete';
import DoneIcon from '@material-ui/icons/Done';

const useStyles = makeStyles({
  table: {
    tableLayout: 'fixed',
    minWidth: 900,
    border: '1px solid gray',
    borderCollapse: 'separate',
  },
  row: {
    '& > *': {
        borderBottom: 'unset',
    },
},
});

const DeleteUser = ({ user, userId, deleteUser }) => {
  const classes = useStyles();
  
  return (
    <TableContainer component={Paper}>
      
    <Table aria-label="collapsible spanning table" className={classes.table}>
      <TableBody>
        <TableRow className={classes.row}>
          <TableCell align="left"> {user.id} </TableCell>
          <TableCell align="justify"> {user.username} </TableCell>
          <TableCell align="justify"> {user.email} </TableCell>
          <TableCell align="center"> {user.is_admin ? 'Admin' : 'User'}</TableCell>        
          <TableCell align="right">
            <Button variant="outlined" type="submit" block onClick={() => deleteUser(userId)}>
              {!user.is_deleted ? <DeleteIcon /> : <DoneIcon />}
            </Button>
          </TableCell>
        </TableRow>
      </TableBody>
      </Table>
    </TableContainer>
  );

};

export default DeleteUser;
