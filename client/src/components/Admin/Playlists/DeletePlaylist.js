import React from 'react';
import { Button, Paper, Table, TableBody, TableCell, TableContainer, TableRow } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import DeleteIcon from '@material-ui/icons/Delete';
import DoneIcon from '@material-ui/icons/Done';
import LibraryMusicIcon from '@material-ui/icons/LibraryMusic';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import QueueMusicIcon from '@material-ui/icons/QueueMusic';

const useStyles = makeStyles({
  table: {
    tableLayout: 'fixed',
    minWidth: 900,
    border: '1px solid gray',
    borderCollapse: 'separate',
  },
  row: {
    '& > *': {
        borderBottom: 'unset',
    },
  },
});

const DeletePlaylist = ({ playlist, playlistId, deletePlaylist }) => {
  const classes = useStyles();
  
  return (
    <TableContainer component={Paper}>
      
    <Table aria-label="collapsible spanning table" className={classes.table}>
      <TableBody>
        <TableRow className={classes.row}>
          <TableCell align="left"> {playlist.id} </TableCell>
          <TableCell align="justify"> <QueueMusicIcon />{playlist.name} </TableCell>
          <TableCell align="justify"> <LibraryMusicIcon /> {(playlist.genre).join(', ')} </TableCell>
          <TableCell align="justify"> <AccountCircleIcon /> {playlist.username} </TableCell>
          <TableCell align="right">
            <Button variant="outlined" type="submit" block onClick={() => deletePlaylist(playlistId)}>
              {!playlist.is_deleted ? <DeleteIcon /> : <DoneIcon />}
            </Button>
          </TableCell>
        </TableRow>
      </TableBody>
      </Table>
    </TableContainer>
  );

};

export default DeletePlaylist;
