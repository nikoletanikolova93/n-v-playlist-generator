import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  button: {
    marginRight: theme.spacing(1),
  },
  instructions: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
}));

const getSteps = () => {
  return ['Select your route', 'Choose your genres', 'Finalize your playlist'];
}

const PlaylistStepper = (props) => {
  const classes = useStyles();
  const steps = getSteps();

  const stepManager = () => {
    if (props.activeStep === 0) {
      return 0
    }
    if (props.activeStep < props.maxGenres) {
      return 1
    }
    return 2
  }

  return (
    <div className={classes.root}>
      <Stepper activeStep={stepManager()}>
        {steps.map((label) => {
          const stepProps = {};
          const labelProps = {};
          return (
            <Step key={label} {...stepProps}>
              <StepLabel {...labelProps}>{label}</StepLabel>
            </Step>
          );
        })}
      </Stepper>
    </div>
  );
};

export default PlaylistStepper;

