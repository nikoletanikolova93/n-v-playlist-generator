import { Fade, Grid, makeStyles, Paper } from '@material-ui/core';
import React from 'react';
import { getRandomElements } from '../../../../common/constants';
import RegularButton from '../../../CustomButtons/Button';

const GenreSelector = (props) => {
    const introArr = ['How about some ', 'How do you feel about ', 'Wanna jam to ', 'Are you in the mood for ', 'Wanna listen to ',
        'I think you might enjoy some ',]
    const useStyles = makeStyles((theme) => ({
        paper: {
            padding: theme.spacing(4),
            textAlign: 'center',
            color: theme.palette.text.secondary,
        },
    }));
    const classes = useStyles();

    const buttonPress = (e) => {
        props.setForm({ ...props.form, [props.genre]: e.currentTarget.value })
        props.setStage(props.stage + 1)
    }

    return <Fade in={props.stage === props.stageNumber} timeout={{ 'appear': 3000, 'enter': 1000 }} mountOnEnter unmountOnExit>
        <Paper className={classes.paper}>
            <Grid container spacing={10} justify="center"
                alignItems="center">
                <Grid justify="center"
                alignItems="center" container item xs={12} spacing={3}>
                    <h1>{getRandomElements(introArr, 1)} {props.genre}?</h1>
                </Grid>
                <Grid justify="center"
                alignItems="center" container item xs={3} spacing={2}>
                    <RegularButton
                        round
                        size='lg'
                        color='rose'
                        value={0}
                        onClick={buttonPress}>Not today</RegularButton>
                </Grid>
                <Grid justify="center"
                alignItems="center" container item xs={3} spacing={2}>
                    <RegularButton
                        round
                        size='lg'
                        color='warning'
                        value={1}
                        onClick={buttonPress}>Just a little</RegularButton>
                </Grid>
                <Grid justify="center"
                alignItems="center" container item xs={3} spacing={2}>
                    <RegularButton
                        round
                        size='lg'
                        color='success'
                        value={2}
                        onClick={buttonPress}>I want a lot</RegularButton>
                </Grid>
            </Grid>
        </Paper>
    </Fade>
};

export default GenreSelector;

