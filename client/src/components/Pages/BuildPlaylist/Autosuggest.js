import React, { useEffect, useState } from 'react';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import CircularProgress from '@material-ui/core/CircularProgress';
import { BING_API_KEY } from '../../../common/constants';

const Autosuggest = (props) => {
    const [open, setOpen] = useState(false);
    const [options, setOptions] = useState([]);
    const [inputValue, setInputValue] = useState('');
    const loading = open && options.length === 0;

    useEffect(() => {
        let active = true;

        if(inputValue) {

            fetch(`http://dev.virtualearth.net/REST/v1/Locations?query=${inputValue}&key=${BING_API_KEY}`)
                .then(res => res.json())
                .then((res) => {
                    if (active) {
                        //setOptions(res.resourceSets[0].resources[0].value.map(e =>e.address.formattedAddress))
                        setOptions(res.resourceSets[0].resources.filter(e => e.confidence !=='Low').map(e => {
                            
                            return {'name': e.name, coordinates: e.geocodePoints[0].coordinates}
                        }))
                    }
                })
                .catch(e => console.log('error'))
        }

            return () => {
                active = false;
              };
    }, [loading, inputValue]);

    useEffect(() => {
        if (!open) {
            setOptions([]);
        }
    }, [open]);

    return (
        <Autocomplete
            id="asynchronous-demo"
            style={{ width: 300 }}
            open={open}
            onOpen={() => {
                setOpen(true);
            }}
            onClose={() => {
                setOpen(false);
            }}
            getOptionSelected={(option) => option.name === inputValue}
            getOptionLabel={(option) => option.name}
            options={options}
            loading={loading}
            onInputChange={e => setInputValue(e.target.value)}
            onChange={(e,option) => option ? props.setCoord({ latitude: option.coordinates[0], longitude: option.coordinates[1] }) : props.setCoord(null)}
            renderInput={(params) => (
                <TextField
                    {...params}
                    label={props.name}
                    variant="outlined"
                    InputProps={{
                        ...params.InputProps,
                        endAdornment: (
                            <>
                                {loading ? <CircularProgress color="inherit" size={20} /> : null}
                                {params.InputProps.endAdornment}
                            </>
                        ),
                    }}
                />
            )}
        />
    );
};

export default Autosuggest;

