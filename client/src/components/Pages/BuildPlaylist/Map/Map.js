/* eslint-disable no-undef */
import { Collapse } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import RegularButton from '../../../CustomButtons/Button';

const Map = (props) => {
    const [showMap, toggleMap] = useState(true)
    const [buttonText, setButtonText] = useState('Click to start planning your route')

    const styles = {
        height: '400px',
        width: '600px',
        position: 'relative',
        cursor: 'crosshair',
        display: showMap ? 'block' : 'none'
    }

    useEffect(() => {
        toggleMap(false)
        setTimeout(() => {
            toggleMap(true)
            GetMap()}
        , 1000)
    }, [])

    const manualAddLocation = () => {
        props.setDuration(0)
        props.setEndCoord(null)
        props.setStartCoord(null)
        const map = resetMap();
        Microsoft.Maps.Events.addHandler(map, 'click', (e) => {
        })
        const addStartPin = Microsoft.Maps.Events.addHandler(map, 'click', (e) => {
            const pin1 = new Microsoft.Maps.Pushpin(new Microsoft.Maps.Location(e.location.latitude, e.location.longitude), {
                title: 'Start'
            })
            map.entities.push(pin1)
            props.setStartCoord({ latitude: e.location.latitude, longitude: e.location.longitude })
            Microsoft.Maps.Events.removeHandler(addStartPin)
            const addEndPin = Microsoft.Maps.Events.addHandler(map, 'click', (e) => {
                const pin2 = new Microsoft.Maps.Pushpin(new Microsoft.Maps.Location(e.location.latitude, e.location.longitude), {
                    title: 'End'
                })
                map.entities.push(pin2)
                props.setEndCoord({ latitude: e.location.latitude, longitude: e.location.longitude })
                Microsoft.Maps.Events.removeHandler(addEndPin)
            })
        })
    };

    const resetMap = () => {
        return new Microsoft.Maps.Map('#myMap', {})
    };

    const handleClick = () => {
        setButtonText('Reset your selection')
        manualAddLocation();
    };

    return (
        <>
            <RegularButton disabled={!showMap} color='info' onClick={() => handleClick()}>{buttonText}</RegularButton>
            <Collapse in={showMap}>
                <div id="myMap" style={styles} />
            </Collapse>
        </>
    )
};
export default Map;
