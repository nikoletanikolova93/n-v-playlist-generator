import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Map from './Map/Map';
import { Button, Container, Fade, Slide } from '@material-ui/core';
import GenreSelector from './GenreSelector/GenreSelector';
import { BASE_URL, BING_API_KEY } from '../../../common/constants';
import PlaylistFinalize from '../../Playlist/PlaylistFinalize';
import PlaylistStepper from './PlaylistStepper/PlaylistStepper';
import { useHistory } from 'react-router';
import Autosuggest from './Autosuggest';
import { displaySeconds } from '../../../common/constants';
import RegularButton from '../../CustomButtons/Button';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(4),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
}));

const BuildPlaylist = () => {
    const classes = useStyles();
    const history = useHistory();
    const [genres, setGenres] = useState([]);
    const [startCoord, setStartCoord] = useState(null);
    const [endCoord, setEndCoord] = useState(null);
    const [duration, setDuration] = useState(0);
    const [stage, setStage] = useState(0);
    const [genreForm, setGenreForm] = useState(null);
    const [playlistName, setPlaylistName] = useState('');
    const [playListPicture, setPlaylistPicture] = useState('');
    const [checkboxSettings, setCheckboxSettings] = useState({
        allowRepeatingArtists: false,
        allowExplicit: true,
    });
    const token = localStorage.getItem("token");

    useEffect(() => {
        fetch(`${BASE_URL}/genres`)
            .then(res => res.json())
            .then(res => setGenres(res))
    }, []);

    const generateRandomForm = (obj) => {
        const randomized = JSON.parse(JSON.stringify(obj));
        if (!Object.values(obj).every(e => +e === 0)) {
            return randomized;
        }
        Object.keys(randomized).forEach(e => randomized[e] = Math.floor(Math.random() * 3));
        return generateRandomForm(randomized);
    };

    const sendPlaylist = () => {

        fetch(`${BASE_URL}/playlists`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token,
            },
            body: JSON.stringify({
                details: {
                    title: playlistName,
                    image: playListPicture,
                },
                options: {
                    'duration': duration,
                    'genres': Object.values(genreForm).every(e => +e === 0) ? { ...generateRandomForm(genreForm) } : { ...genreForm },
                    ...checkboxSettings,
                }
            }),
        })
            .then(res => res.json())
            .then(res => history.push(`/playlists/${res.playlist[0].id}`))
            .catch(e => console.error(e))
    };

    const getDistance = (start, end) => {
        fetch(`https://dev.virtualearth.net/REST/v1/Routes/DistanceMatrix?origins=${start.latitude},${start.longitude}&destinations=${end.latitude},${end.longitude}&travelMode=driving&timeUnit=second&key=${BING_API_KEY}`)
            .then(res => res.json())
            .then(res => setDuration(res.resourceSets[0].resources[0].results[0].travelDuration))
            .catch(e => console.log(e))
    };

    return (
        <Container component="main" maxWidth="xl">
            {genres ? <PlaylistStepper activeStep={stage} setActiveStep={setStage} maxGenres={genres.length + 1} /> : null}
            <div className={classes.root}>
                <Slide direction="left" in={stage === 0} mountOnEnter unmountOnExit>
                    <Grid container spacing={3} >
                        <Grid item xs={6}>
                            <Paper className={classes.paper}>
                                <h2>Select your start point and destination</h2>
                                <h3>Use the map to click on your locations</h3>
                                <Map setDuration={setDuration}
                                    setEndCoord={setEndCoord}
                                    setStartCoord={setStartCoord}
                                    startCoord={startCoord} />
                            </Paper>
                        </Grid>
                        <Grid item xs={6}>
                            <Paper className={classes.paper}>
                                <Grid container spacing={3} justify="center">
                                    <Grid item xl={6}>
                                        <h2>Or you can type your start point and destination</h2>
                                    </Grid>
                                    <Grid item>
                                        <Autosuggest name='Start destination'
                                            setCoord={setStartCoord} />
                                    </Grid>
                                    <Grid item>
                                        <Autosuggest name='End destination'
                                            setCoord={setEndCoord} />
                                    </Grid>
                                </Grid>
                            </Paper>
                            <Grid item>
                                <Paper className={classes.paper}>
                                    <RegularButton
                                        color="info"
                                        disabled={!endCoord}
                                        onClick={() => getDistance(startCoord, endCoord)}>Calculate my trip</RegularButton>
                                    {duration ? (duration < 0 ? <h3>Sadly, your route is impossible. Please select a new one.</h3> : (<><h2>{displaySeconds(duration)}</h2>
                                        <h3>Great, now let's choose what genres you want to listen to</h3>
                                        <RegularButton
                                        round
                                        color="primary"
                                        disabled={!(duration && duration > 0 )}
                                        onClick={() => setStage(stage + 1)}>Next</RegularButton> </>)) : null}
                                </Paper>
                            </Grid>
                        </Grid>
                    </Grid>
                </Slide>
                {genres ? genres.map(e => <GenreSelector key={e.id} genre={e.name} stage={stage} stageNumber={genres.indexOf(e) + 1} setStage={setStage} setForm={setGenreForm} form={genreForm} />) : null}
                <Grid container spacing={3}>
                    <Grid item sm={12}>
                        {genreForm && Object.values(genreForm).every(e => +e === 0) ? (
                            <Fade in={stage === (genres.length && genres.length + 1)} timeout={{ 'appear': 3000, 'enter': 1000 }} mountOnEnter unmountOnExit>
                                <Paper className={classes.paper}>
                                    <h2>Whoa, you haven't selected any genres!</h2>
                                    <h3>You can go back and choose some or you can continue and we'll pick some for you</h3>
                                    <Button variant="contained" color='primary' onClick={() => setStage(1)}>Send me back</Button>
                                </Paper>
                            </Fade>) : null}
                        <Fade in={stage === (genres.length && genres.length + 1)} timeout={{ 'appear': 3000, 'enter': 1000 }} mountOnEnter unmountOnExit>
                            <Paper className={classes.paper}>
                                <PlaylistFinalize
                                    playlistName={playlistName}
                                    setPlaylistName={setPlaylistName}
                                    setPlaylistPicture={setPlaylistPicture}
                                    checkboxSettings={checkboxSettings}
                                    setCheckboxSettings={setCheckboxSettings}
                                />
                                <Button variant="contained" disabled={!playListPicture || playlistName.length < 3} onClick={() => sendPlaylist()}>Send playlist</Button>
                            </Paper>
                        </Fade>
                    </Grid>
                </Grid>
            </div>
        </Container>
    );
};
export default BuildPlaylist;
