import { Typography } from '@material-ui/core';
import React from 'react';

const NotFound = () => {
  
  return (
    <div>
      <Typography variant="h3" noWrap style={{textAlign: "center"}}>
        Oops... Page not found!
      </Typography>
    </div>
  );
};

export default NotFound;
