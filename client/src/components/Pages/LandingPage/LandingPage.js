import React, { useContext } from 'react';
// nodejs library that concatenates classes
import classNames from 'classnames';
// @material-ui/core components
import { makeStyles } from '@material-ui/core/styles';
import { useHistory } from 'react-router-dom';
import styles from './assets/landingPageStyles'
import GridContainer from './components/Grid/GridContainer';
import GridItem from './components/Grid/GridItem';
import Parallax from './components/Parallax/Parallax';
import ProductSection from './Sections/ProductSection';
import TeamSection from './Sections/TeamSection';
import RegularButton from '../../CustomButtons/Button';
import Swal from 'sweetalert2';
import AuthContext from '../../../providers/authContext';

const useStyles = makeStyles(styles);

export default function LandingPage(props) {
    const classes = useStyles();
    const history = useHistory()
    //const { ...rest } = props;
    const { isLoggedIn } = useContext(AuthContext);

    const handleMainClick = () => {
        if (isLoggedIn) {
            history.push(`/build-a-playlist`)
        } else {
            Swal.fire({
                icon: 'info',
                text: 'You must be logged in to create a playlist',
                showDenyButton: true,
                confirmButtonText: `Login`,
                denyButtonText: `Register`,
                denyButtonColor: '#3085d6',
                footer: `<p>Creating an account is easy and takes no time at all!</p>`
            }).then((result) => {
                if (result.isConfirmed) {
                    history.push('/login')
                } else if (result.isDenied) {
                    history.push('/register')
                }
            })
        }
    }

    return (
        <div>
            <Parallax filter image={require("../../../assets/img/averie-woodard.jpg").default}>
                <div className={classes.container}>
                    <GridContainer >
                        <GridItem xs={12} sm={12} md={6}>
                            <h1 className={classes.title}>Your ride, your music</h1>
                            <h4>
                                Every journey is unique, every roadtrip is a memory. Make the best memories with the best music.
                </h4>
                            <RegularButton
                                color='info'
                                size='lg'
                                onClick={handleMainClick}>
                                Create your playlist
                </RegularButton>
                        </GridItem>
                    </GridContainer>
                </div>
            </Parallax>
            <div className={classNames(classes.main, classes.mainRaised)}>
                <div className={classes.container}>
                    <ProductSection />
                    <TeamSection />
                </div>
            </div>
        </div>
    );
};
