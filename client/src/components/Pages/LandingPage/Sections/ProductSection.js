import React from 'react';
// @material-ui/core components
import { makeStyles } from '@material-ui/core/styles';
import styles from '../../../../assets/jss/material-kit-react/views/landingPageSections/productStyle.js';
import GridContainer from '../components/Grid/GridContainer.js';
import GridItem from '../components/Grid/GridItem.js';
import PlaylistsPreview from '../../../Playlist/PlaylistsPreview.js';

const useStyles = makeStyles(styles);

export default function ProductSection() {
  const classes = useStyles();
  return (
    <div className={classes.section}>
      <GridContainer justify="center">
        <GridItem xs={12} sm={12} md={8}>
          <h2 className={classes.title}>A playlist for everyone</h2>
          <h4 className={classes.description}>
          Whether you're going to work or on vacation, alone or with friends; take the music with you.
          Create a playlist and make it your own.
          It's as easy as selecting your destination and personalizing it with the genres you want to listen to.
          <br/>
          In the meantime, why not enjoy our highest rated playlists.
          </h4>
        </GridItem>
      </GridContainer>
      <div>
        <GridContainer>
          <GridItem style={{display: 'inline-block'}}>
            <PlaylistsPreview />
          </GridItem>
        </GridContainer>
      </div>
    </div>
  );
};
