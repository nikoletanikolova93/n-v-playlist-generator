import React from 'react';
// nodejs library that concatenates classes
import classNames from 'classnames';
// @material-ui/core components
import { makeStyles } from '@material-ui/core/styles';
// core components
import Card from '../components/Card/Card.js'
import CardFooter from '../components/Card/CardFooter.js'
import styles from "../../../../assets/jss/material-kit-react/views/landingPageSections/teamStyle.js";
import GridContainer from "../components/Grid/GridContainer.js";
import GridItem from "../components/Grid/GridItem.js";
import { Button } from "@material-ui/core";
import team1 from '../../../../assets/img/faces/team1.png';
import team2 from '../../../../assets/img/faces/team2.jpg';

const useStyles = makeStyles(styles);

export default function TeamSection() {
  const classes = useStyles();
  const imageClasses = classNames(
    classes.imgRaised,
    classes.imgRoundedCircle,
    classes.imgFluid
  );
  return (
    <div className={classes.section}>
      <h2 className={classes.title}>The team</h2>
      <div>
        <GridContainer>
          <GridItem xs={12} sm={12} md={6}>
            <Card plain>
              <GridItem xs={12} sm={12} md={6} className={classes.itemGrid}>
                <img src={team1} alt="..." style={{ width: "120px", height: "120px" }} className={imageClasses} />
              </GridItem>
              <h4 className={classes.cardTitle}>
                Vladislav Antonov
                <br />
                <small className={classes.smallTitle}>JavaScript Developer</small>
              </h4>
              <CardFooter className={classes.justifyCenter}>
                <Button
                  className={classes.margin5}
                >
                  <a href="https://gitlab.com/antonov.vl">

                    <i className={classes.socials + " fab fa-gitlab"} />
                  </a>
                </Button>
              </CardFooter>
            </Card>
          </GridItem>
          <GridItem xs={12} sm={12} md={6}>
            <Card plain>
              <GridItem xs={12} sm={12} md={6} className={classes.itemGrid}>
                <img src={team2} alt="avatar" style={{ width: "120px", height: "120px" }} className={imageClasses} />
              </GridItem>
              <h4 className={classes.cardTitle}>
                Nikoleta Nikolova
                <br />
                <small className={classes.smallTitle}>JavaScript Developer</small>
              </h4>
              <CardFooter className={classes.justifyCenter}>
                <Button
                  className={classes.margin5}
                >
                  <a href="https://gitlab.com/nikoletanikolova93">
                    <i className={classes.socials + " fab fa-gitlab"} />
                  </a>
                </Button>
              </CardFooter>
            </Card>
          </GridItem>
        </GridContainer>
      </div>
    </div>
  );
};
