import React, { useContext, useState } from 'react';
import { NavLink, useHistory, withRouter } from 'react-router-dom';
import { fade, makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import InputBase from '@material-ui/core/InputBase';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import SearchIcon from '@material-ui/icons/Search';
import AccountCircle from '@material-ui/icons/AccountCircle';
import MusicVideoOutlinedIcon from '@material-ui/icons/MusicVideoOutlined';
import MoreIcon from '@material-ui/icons/MoreVert';
import AuthContext from '../../../providers/authContext';
import Logout from '../../../components/User/Logout/Logout';
import PlaylistPlayIcon from '@material-ui/icons/PlaylistPlay';
import AddIcon from '@material-ui/icons/Add';

const useStyles = makeStyles((theme) => ({
  grow: {
    flexGrow: 1,
    paddingTop: '50px',
    // paddingTop: theme.spacing(1),
    // paddingBottom: theme.spacing(2),
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block',
    },
    fontSize: 18,
    fontFamily: 'Segoe UI',
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(3),
      width: 'auto',
    },
    fontSize: 18,
    fontFamily: 'Segoe UI',
  },
  searchIcon: {
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputRoot: {
    color: 'inherit',
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: '20ch',
    },
    fontSize: 16,
    fontFamily: 'Segoe UI',
  },
  sectionDesktop: {
    display: 'none',
    [theme.breakpoints.up('md')]: {
      display: 'flex',
    },
    color: 'inherit',
  },
  sectionMobile: {
    display: 'flex',
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
  appbar: {
    width: '100%',
    height: '60px',
    paddingTop: '0px',
    paddingBottom: '0px',
    background: '#004D84',
  },
}));

const Header = () => {
  const [search, setSearch] = useState('');
  const history = useHistory();
  const classes = useStyles();
  const { isLoggedIn, isAdmin, user} = useContext(AuthContext);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = React.useState(null);

  const isMenuOpen = Boolean(anchorEl);
  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);

  const handleProfileMenuOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMobileMenuClose = () => {
    setMobileMoreAnchorEl(null);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
    handleMobileMenuClose();
  };

  const handleMobileMenuOpen = (event) => {
    setMobileMoreAnchorEl(event.currentTarget);
  };

  const menuId = 'primary-search-account-menu';
  const renderMenu = (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      id={menuId}
      keepMounted
      transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      open={isMenuOpen}
      onClose={handleMenuClose}
    >
      {isLoggedIn && 
      <MenuItem onClick={handleMenuClose}>
        {<NavLink to="/users/profile" style={{ color: 'black' }}>
          My profile
        </NavLink>}
      </MenuItem>}

      {isLoggedIn && isAdmin && 
      <MenuItem onClick={handleMenuClose}>
        {<NavLink to="/admin/playlists" style={{ color: 'black' }}>
          Admin Playlists
        </NavLink>}
      </MenuItem>}

      {isLoggedIn && isAdmin && 
      <MenuItem onClick={handleMenuClose}>
        {<NavLink to="/admin/users" style={{ color: 'black' }}>
          Admin Users
        </NavLink>}
      </MenuItem>}

      {isLoggedIn && 
      <MenuItem onClick={handleMenuClose}>
        {<Logout />}
      </MenuItem>}
    </Menu>
  );

  const mobileMenuId = 'primary-search-account-menu-mobile';
  const renderMobileMenu = (
    <Menu
      anchorEl={mobileMoreAnchorEl}
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      id={mobileMenuId}
      keepMounted
      transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      open={isMobileMenuOpen}
      onClose={handleMobileMenuClose}
    >
      <MenuItem onClick={handleProfileMenuOpen}>
        <IconButton
          aria-label="account of current user"
          aria-controls="primary-search-account-menu"
          aria-haspopup="true"
          color="inherit"
        >
          <AccountCircle />
        </IconButton>
      </MenuItem>
    </Menu>
  );


  return (
    <div className={classes.grow}>
      <AppBar className={classes.appbar} position="fixed" >
        <Toolbar>
          <IconButton
            edge="start"
            color="inherit"
            onClick={() => history.push(`/home`)}
          >
            <MusicVideoOutlinedIcon />
          </IconButton>
          <Typography className={classes.title} variant="h6" noWrap style={{cursor: "pointer"}}

          onClick={() => history.push(`/home`)}>
            Ridepal
          </Typography>
          <div className={classes.search}>
            <div className={classes.searchIcon}>
              <SearchIcon />
            </div>
            <InputBase
              placeholder="Search…"
              classes={{
                root: classes.inputRoot,
                input: classes.inputInput,
              }}
              inputProps={{ 'aria-label': 'search' }}
              type="text" className="mr-sm-2" 
              onSubmit={e => {e.preventDefault()}} 
              onChange={e => setSearch(e.target.value)}
              onKeyUp={() => {
                if(search.length >= 2) history.push(`/playlists?name=${search}`)}}
            />
          </div>
          <div className={classes.grow} />
          <div className={classes.sectionDesktop}>
          {!isLoggedIn && <IconButton
            edge="start"
            color="inherit"
            onClick={() => history.push(`/login`)}
          >
            <Typography className={classes.title} variant="h6" noWrap>
              Sign in
            </Typography>
          </IconButton>}
          {!isLoggedIn && <IconButton
            edge="start"
            color="inherit"
            onClick={() => history.push(`/register`)}
          >
            <Typography className={classes.title} variant="h6" noWrap>
              Sign up
            </Typography>
          </IconButton>}
          <br></br>
          {isLoggedIn  && <IconButton
            edge="start"
            aria-label="build-playlist"
            aria-controls={menuId}
            aria-haspopup="true"
            color="inherit"
            onClick={() => history.push(`/build-a-playlist`)}
          >
            <AddIcon />
          </IconButton>}
          <IconButton
            edge="start"
            aria-label="view-all-playlists"
            aria-controls={menuId}
            aria-haspopup="true"
            color="inherit"
            onClick={() => history.push(`/playlists`)}
          >
            <PlaylistPlayIcon />
          </IconButton>
            <IconButton
              edge="end"
              aria-label="account of current user"
              aria-controls={menuId}
              aria-haspopup="true"
              onClick={handleProfileMenuOpen}
              color="inherit"
            >
              <Typography className={classes.title} variant="h6" noWrap>
                {isLoggedIn 
                ? (isLoggedIn && user && ` ${user ? user.username : null}` ) 
                : <AccountCircle />}
              </Typography>
            </IconButton>
          </div>
          <div className={classes.sectionMobile}>
            <IconButton
              aria-label="show more"
              aria-controls={mobileMenuId}
              aria-haspopup="true"
              onClick={handleMobileMenuOpen}
              color="inherit"
            >
              <MoreIcon />
            </IconButton>
          </div>
        </Toolbar>
      </AppBar>
      {isLoggedIn ? renderMobileMenu : null}
      {isLoggedIn ? renderMenu : null}
    </div>
  );
};

export default withRouter(Header);
