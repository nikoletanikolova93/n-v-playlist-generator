import React from "react";
import './Footer.css'

const Footer = () => {
  return (
    <div className="footer" >
      <p>Copyright &#169; 2021 RidePal. All rights reserved.</p>
    </div>
  );
};

export default Footer;
