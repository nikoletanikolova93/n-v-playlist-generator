import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Button, Card, CardActionArea, CardActions, CardContent, CardMedia, Typography } from '@material-ui/core';
import { useHistory } from 'react-router';
import { BASE_URL, displaySeconds } from '../../common/constants';
import TimelapseIcon from '@material-ui/icons/Timelapse';
import TrendingUpIcon from '@material-ui/icons/TrendingUp';
import LibraryMusicIcon from '@material-ui/icons/LibraryMusic';
import { getToken, getUser } from '../../providers/authContext';
import Swal from 'sweetalert2';

const useStyles = makeStyles({
  root: {
    maxWidth: 345,
    padding: 6,
    margin: 3,
  },
  media: {
    height: 170,
    width: 285,
  },
});

const PlaylistCard = ({ playlist, detailed }) => {
  const classes = useStyles();
  const history = useHistory();
  const duration = displaySeconds(playlist.duration);
  // eslint-disable-next-line no-unused-vars
  const [deleted, setDeleted] = useState(false);
  const activeUser = getUser() ? getUser().id : null;

  const deletePlaylist = () => {
    try {
      fetch(`${BASE_URL}/playlists/${playlist.id}`, {
        method: 'DELETE',
        headers: {
          Authorization: `Bearer ${getToken()}`,
        },
      })
        .then(r => r.json())
        .then(() => setDeleted(true))
        .catch((error) => console.log(error))
    } catch (error) {
      console.log(error);
    }
  };

  if (deleted) {
    return null;
  }

  return (
    <Card className={classes.root} style={{display: 'inline-block'}}>
      <CardActionArea style={{ textAlign: "center" }}>
        <CardMedia
          className={classes.media}
          image={playlist.image}
          title="Playlist Image"
          onClick={!detailed ? () => history.push(`/playlists/${playlist.id}`) : null}
          />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            {playlist.name}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            <TimelapseIcon />
            {duration} {' '}
            <TrendingUpIcon />
            {playlist.rank}
            <br></br>
            <LibraryMusicIcon />
            {(playlist.genre).join(', ')}
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions position="absolute">
        {!detailed ? <Button size="small" variant="outlined" onClick={() => history.push(`/playlists/${playlist.id}`)}>
          Details
          </Button> : null}

          {playlist.user_id === activeUser
          ? (<Button size="small" variant="outlined" onClick={() => { 
            if (Swal.fire(
              'Deleted!',
              'Your playlist has been deleted.',
              'success'
              )
            )
            deletePlaylist()} }>Delete</Button>) 
          : null }
      </CardActions>
    </Card>
  );
};

export default PlaylistCard;
