import React, { useContext, useEffect, useState } from 'react';
import { BASE_URL } from './../../common/constants';
import AuthContext, { getToken } from '../../providers/authContext';
import { makeStyles } from '@material-ui/core/styles';
import { Container, IconButton, Typography } from '@material-ui/core';
import PlaylistCard from './PlaylistCard';
import AddIcon from '@material-ui/icons/Add';
import { useHistory } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
}));

const PlaylistsPreview = () => {
  
  const classes = useStyles();
  const [allPlaylists, setAllPlaylists] = useState([]);
    // eslint-disable-next-line
  const [error, setError] = useState(null);
  const queryParams = window.location.search;

  useEffect(() => {
    fetch(`${BASE_URL}/preview`, {
      headers: {
        Authorization: `Bearer ${getToken()}`,
      },
    })
    .then(res => res.json())
    .then(res => {
      if (Array.isArray(res)) {
        setAllPlaylists(res);
    } else {
        setError('There are no playlists!');
    }})
  }, [queryParams]);

  return (
      <div className={classes.root}>
        <Container>
          {allPlaylists.map(p => (<PlaylistCard playlist={p} key={p.id} />))}
        </Container>
      </div>
  );
};

export default PlaylistsPreview;
