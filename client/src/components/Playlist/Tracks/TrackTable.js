import { makeStyles, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from '@material-ui/core';
import React from 'react';
import TrackRow from './TrackRow';
import PersonIcon from '@material-ui/icons/Person';
import AlbumIcon from '@material-ui/icons/Album';
import MusicNoteIcon from '@material-ui/icons/MusicNote';
import TimelapseIcon from '@material-ui/icons/Timelapse';
import RadioIcon from '@material-ui/icons/Radio';
import { displaySeconds } from '../../../common/constants';


const useStyles = makeStyles((theme) => ({
  icon: {
    'vertical-align': 'bottom',
  },
}));

const TrackTable = (props) => {

  const { tracklist } = props
  const classes = useStyles()

  return (
    <TableContainer component={Paper}>
      <Table aria-label="collapsible spanning table">
        <TableHead>
          <TableRow>
            <TableCell />
            <TableCell><MusicNoteIcon fontSize="large" className={classes.icon} /> Track name</TableCell>
            <TableCell align="right"><PersonIcon fontSize="large" className={classes.icon} /> Artist</TableCell>
            <TableCell align="right"><AlbumIcon fontSize="large" className={classes.icon} /> Album</TableCell>
            <TableCell align="right"><RadioIcon fontSize="large" className={classes.icon} /> Genre</TableCell>
            <TableCell align="right"><TimelapseIcon fontSize="large" className={classes.icon} /> Duration</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {tracklist.map((track) => (
            <TrackRow key={track.id} row={track} />
          ))}
          <TableRow>
            <TableCell align="right" colSpan={5}>Total duration</TableCell>
            <TableCell align="right">{displaySeconds(tracklist.reduce((acc, curVal) => acc + curVal.duration, 0))}</TableCell>
          </TableRow>
        </TableBody>
      </Table>
    </TableContainer>
  );
}

export default TrackTable
