import React, { useState } from 'react';
import { Box, Collapse, IconButton, makeStyles, TableCell, TableRow, Typography } from '@material-ui/core';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import H5AudioPlayer from 'react-h5-audio-player';
import 'react-h5-audio-player/lib/styles.css';
import { displaySeconds } from '../../../common/constants';
import ExplicitIcon from '@material-ui/icons/Explicit';

const useRowStyles = makeStyles({
    root: {
        '& > *': {
            borderBottom: 'unset',
        },
    },
    icon: {
        'vertical-align':'bottom',
    },
});

const TrackRow = (props) => {
    const classes = useRowStyles();
    const { row } = props;
    const [open, setOpen] = useState(false);

    return (
        <>
            <TableRow className={classes.root}>
                <TableCell>
                    <IconButton aria-label="expand row" size="small" onClick={() => setOpen(!open)}>
                        {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                    </IconButton>
                </TableCell>
                <TableCell component="th" scope="row">
                    {row.title} 
                    {row.explicit_lyrics ?
                <ExplicitIcon className={classes.icon}
                color="action"/> : null}
                </TableCell>
                <TableCell align="right">{row.artist_name}</TableCell>
                <TableCell align="right">{row.album_title}</TableCell>
                <TableCell align="right">{row.genre}</TableCell>
                <TableCell align="right">{displaySeconds(row.duration)}</TableCell>
            </TableRow>
            <TableRow>
                <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
                    <Collapse in={open} timeout="auto" unmountOnExit>
                        <Box margin={1}>
                            <Typography variant="h5" gutterBottom component="div">
                                Preview track
                            </Typography>
                            <H5AudioPlayer
                                src={row.preview}
                                showJumpControls={false}
                                customAdditionalControls={[]}
                                volume={0.5}
                            />
                        </Box>
                    </Collapse>
                </TableCell>
            </TableRow>
        </>
    );
};

export default TrackRow;
