import React, { useContext, useEffect, useState } from 'react';
import { BASE_URL } from './../../common/constants';
import AuthContext, { getToken } from '../../providers/authContext';
import { makeStyles } from '@material-ui/core/styles';
import { Container, IconButton, Typography } from '@material-ui/core';
import PlaylistCard from './PlaylistCard';
import AddIcon from '@material-ui/icons/Add';
import { useHistory } from 'react-router-dom';
import Loader from '../Pages/Loader/Loader';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
}));

const AllPlaylists = () => {
  
  const classes = useStyles();
  const [allPlaylists, setAllPlaylists] = useState([]);
    // eslint-disable-next-line
  const [error, setError] = useState(null);
  const queryParams = window.location.search;
  const history = useHistory();
  const { isLoggedIn } = useContext(AuthContext);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);

    fetch(`${BASE_URL}/playlists${queryParams}`, {
      headers: {
        Authorization: `Bearer ${getToken()}`,
      },
    })
    .then(res => res.json())
    .then(res => {
      if (Array.isArray(res)) {
        setAllPlaylists(res);
    } else {
        setError('There are no playlists!');
    }})
    .finally(() => setLoading(false));
  }, [queryParams]);

  if (loading) {
    return (
        <Loader>
            <Typography variant="h5">Loading playlists, please wait...</Typography>
        </Loader>
    );
  }

  return (
      <div className={classes.root}>
        <Container>
          <br></br>
        <Typography variant="h5" style={{ textAlign: "center" }} >
          {allPlaylists.length ? 'All Playlists' : 'No results found'}
        <br></br>
        {isLoggedIn && allPlaylists.length !== 0 && <IconButton 
            edge="start"
            color="inherit"
            onClick={() => history.push(`/build-a-playlist`)}
          >
            <AddIcon />
          </IconButton>}
        </Typography>
          {allPlaylists.map(p => (<PlaylistCard playlist={p} key={p.id} />))}
          <br></br>
        </Container>
      </div>
  );
};

export default AllPlaylists;
