import { makeStyles } from '@material-ui/core';
import React from 'react';

const useStyles = makeStyles({
    image: {
        height: 170,
        width: 285,
        boxShadow:
            "0 5px 15px -8px rgba(0, 0, 0, 0.24), 0 8px 10px -5px rgba(0, 0, 0, 0.2)",
        '&:hover': {
            cursor:'pointer',
            opacity:'0.8'
        },
    },
    selected: {
        opacity:'0.6',
    },
});

const SamplePicture = (props) => {
    const classes = useStyles()

    const handleClick = () => {
        props.setPlaylistPicture(props.picture.webformatURL)
        props.setActivePicture(props.picture.id)

    }

    return <img
        className={classes.image + ` ${props.activePicture && props.activePicture !== props.picture.id ? classes.selected : null}`}
        src={props.picture.webformatURL}
        onClick={handleClick}
        alt='Sample' />
};
export default SamplePicture;

