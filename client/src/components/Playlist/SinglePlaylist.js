import React, { useEffect, useState } from 'react';
import { useParams, useHistory } from 'react-router';
import { BASE_URL } from '../../common/constants';
import { getToken } from '../../providers/authContext';
import PlaylistDetails from './PlaylistDetails/PlaylistDetails';
import { makeStyles } from '@material-ui/core/styles';
import { Container, Grid } from '@material-ui/core';
import TrackTable from './Tracks/TrackTable';
import Skeleton from '@material-ui/lab/Skeleton';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
  details: {
    flexDirection: "column"
  }
}));

const SinglePlaylist = () => {

  const classes = useStyles();
  const [singlePlaylist, setSinglePlaylist] = useState({});
  const [loading, setLoading] = useState(true)
  const { playlistId } = useParams();
  const history = useHistory();

  useEffect(() => {
    fetch(`${BASE_URL}/playlists/${playlistId}`, {
      headers: {
        'Authorization': `Bearer ${getToken()}`,
      },
    })
      .then(res => {
        if (res.ok) {
          return res.json();
        }
        return history.push('/home');
      })
      .then(res => {
        setSinglePlaylist(res)
      })
      .catch(error => {
        console.log(error);
      })
  }, [playlistId, history]);

  useEffect(() => {
    if (Object.keys(singlePlaylist).length) {
      setLoading(false)
    }
  }, [singlePlaylist, setLoading])

  return (
    <Container maxWidth="lg">
      <div className={classes.root}>
        <Container maxWidth='lg'>
          <Grid container
            alignItems="center" direction="row" spacing={1}>
            {!loading ? <PlaylistDetails playlist={singlePlaylist.playlist[0]} tracks={singlePlaylist.tracks} /> : <Skeleton variant="rect" width={250} height={250} />}
          </Grid>
        </Container>
        {!loading
          ? <TrackTable tracklist={singlePlaylist.tracks} /> : null}
      </div>
    </Container>
  );
};

export default SinglePlaylist;
