import React, { useEffect, useState } from 'react';
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';
import { Button, Checkbox, Container, FormControl, FormControlLabel, FormGroup, Grid } from '@material-ui/core';
import { getRandomElements, PIXABAY_API_KEY } from '../../common/constants';
import SamplePicture from './SamplePicture';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
    },
}));


const PlaylistFinalize = (props) => {
    
    const classes = useStyles();
    const [samplePictures, setSamplePictures] = useState([])
    const [activePicture, setActivePicture] = useState(null)
    const pictureKeywords = ['music', 'sound', 'vacation', 'journey', 'trip', 'dance', 'road']
    
    useEffect(() => {
        fetchPictures()
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const fetchPictures = () => {
        setActivePicture(null)
        fetch(`https://pixabay.com/api/?key=${PIXABAY_API_KEY}&q=${getRandomElements(pictureKeywords, 1)}&per_page=30`)
            .then(res => res.json())
            .then(res => setSamplePictures(getRandomElements(res.hits, 3)))
    }

    const handleClick = (event) => {
        props.setCheckboxSettings({ ...props.checkboxSettings, [event.target.name]: !props.checkboxSettings[event.target.name] })
    }

    return <Container>
        <h2>Select an image for your playlist</h2>
        <Grid container spacing={3} justify="center"
                alignItems="center">
            {samplePictures.length > 0 ? samplePictures.map(e => (<Grid item container xs={3}>
                <SamplePicture key={e.id}
                    picture={e}
                    setPlaylistPicture={props.setPlaylistPicture}
                    activePicture={activePicture}
                    setActivePicture={setActivePicture} />
            </Grid>)) : null}
            <Grid item justify="center"
                alignItems="center" container xl={12}>
                <Button variant="contained"
                    color='primary'
                    onClick={fetchPictures}>Give me a new selection</Button>
            </Grid>
        </Grid>
        <h2>Give your playlist a cool name</h2>
        <form className={classes.root} noValidate autoComplete="off">
            <TextField
                required
                id="standard-required"
                label="Playlist name"
                value={props.playlistName}
                onChange={e => props.setPlaylistName(e.target.value)} />
        </form>
        <FormControl component="fieldset">
            <FormGroup aria-label="position">
                <FormControlLabel
                    value="end"
                    control={<Checkbox
                        color="primary"
                        name='allowRepeatingArtists'
                        checked={props.checkboxSettings.allowRepeatingArtists}
                        onClick={handleClick} />}
                    label="Allow duplicate artists"
                    labelPlacement="end"
                />
                <FormControlLabel
                    value="end"
                    control={<Checkbox
                        color="primary"
                        name='allowExplicit'
                        checked={props.checkboxSettings.allowExplicit}
                        onClick={handleClick}
                    />}
                    label="Allow songs with explicit lyrics"
                    labelPlacement="end"
                />
            </FormGroup>
        </FormControl>
    </Container>

};

export default PlaylistFinalize
