import React from 'react';
import { Button, Grid, makeStyles } from '@material-ui/core';
import { displaySeconds } from '../../../common/constants';
import ExplicitIcon from '@material-ui/icons/Explicit';
import TrendingUpIcon from '@material-ui/icons/TrendingUp';
import LibraryMusicIcon from '@material-ui/icons/LibraryMusic';
import SearchIcon from '@material-ui/icons/Search';
import { useHistory } from 'react-router';

const useStyles = makeStyles({
  root: {
    maxWidth: 345,
    padding: 6,
    margin: 3,
  },
  media: {
    width: '250px',
    height: '250px',
    'object-fit': 'cover',
  },
  imgRoundedCircle: {
    borderRadius: "50% !important",
  },
  imgRaised: {
    boxShadow:
      "0 5px 15px -8px rgba(0, 0, 0, 0.24), 0 8px 10px -5px rgba(0, 0, 0, 0.2)",
  },
  title: {
    display: "inline-block",
    position: "relative",
    marginTop: "30px",
    minHeight: "32px",
    textDecoration: "none",
    'font-size':'70px',
    'font-weight': 'bold',
  },
  details: {
    'font-weight': 'bold',
  },
  icon: {
    'vertical-align':'bottom',
  },
});

const PlaylistDetails = ({ playlist, tracks }) => {
  const classes = useStyles();
  const history = useHistory();
  return <>
    <Grid item xs ={3}>
      <img className={classes.media + ' ' + classes.imgRaised}
        src={playlist.image}
        alt='' />
    </Grid>
    <Grid item xs>
        <h1 className={classes.title}>
        {playlist.name}
        </h1>
        <h5 className={classes.details}>
          {playlist.username} • {tracks.length + ' songs'} • {displaySeconds(playlist.duration)}
        </h5>
        <h6 className={classes.details}>
          <LibraryMusicIcon className={classes.icon}/> {playlist.genre.join(' • ')}
        </h6>
        <h6 className={classes.details + ' ' }>
          <TrendingUpIcon className={classes.icon}/> Rank points: {playlist.rank}
        </h6>
        <h6 className={classes.details}>
          {tracks.some(t => t.explicit_lyrics !== 0) ? (<><ExplicitIcon className={classes.icon}/> Explicit content</>) : null}
        </h6>
    </Grid>
    <Grid item xl>
      <Button variant='contained'
      startIcon={<SearchIcon />}
      onClick={()=>history.push(`/playlists?duration=${playlist.duration}`)}>Find similar</Button>
    </Grid>
  </>
};

export default PlaylistDetails;
