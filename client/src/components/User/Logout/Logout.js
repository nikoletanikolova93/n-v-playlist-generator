import React, { useContext } from 'react';
import authContext, { getToken } from '../../../providers/authContext';
import { BASE_URL } from '../../../common/constants';
import { Button, Container } from '@material-ui/core';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import { useHistory } from 'react-router-dom';

const Logout = () => {

  const { setAuthState } = useContext(authContext);
  const history = useHistory();

  const logout = (ev) => {
    ev.preventDefault();
    
    fetch(`${BASE_URL}/logout`, {
      method: 'POST',
      headers: {
      Authorization: `Bearer ${getToken()}`,
    },
  })
    .then(r => r.json())
    .then(r => {

    localStorage.removeItem('token');

    setAuthState({
      isLoggedIn: false,
      user: null
    });
    history.push('/home');
  })
    .catch(console.warn);
  };
      
  return (
    <Container>
      <Button size="small" type="submit" onClick={logout}>
        <ExitToAppIcon />
      </Button>
    </Container>
  );

};

export default Logout;
