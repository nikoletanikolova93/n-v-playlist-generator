import React, { useContext, useState } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import AuthContext from '../../../providers/authContext';
import { BASE_URL, EMAIL_VALIDATION, PASSWORD_VALIDATION } from '../../../common/constants';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import PermIdentityIcon from '@material-ui/icons/PermIdentity';
import decode from 'jwt-decode';
import { IconButton, InputAdornment } from '@material-ui/core';
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import image from '../../../assets/img/login-background.jpg';

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'inline-flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.primary.main,
  },
  form: {
    width: '100%',
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  pageHeader: {
    minHeight: "89vh",
    height: "auto",
    display: "inherit",
    position: "relative",
    margin: "0",
    padding: "0",
    border: "0",
    alignItems: "center",
    "&:before": {
      background: "rgba(0, 0, 0, 0.5)",
    },
  },
}));

export const Login = (props) => {
  const classes = useStyles();
  const auth = useContext(AuthContext);
  const [showPassword, setShowPassword] = useState(false);
  const handleClickShowPassword = () => setShowPassword(!showPassword);
  const handleMouseDownPassword = () => setShowPassword(!showPassword);

  const [user, setUser] = useState({
    email: {
      value: '',
      touched: false,
      valid: false,
    },
    password: {
      value: '',
      touched: false,
      valid: false,
    },
  });

  const loginValidators = {
    email: [
      value => value.match(EMAIL_VALIDATION) || ''],
    password: [
      value => value.match(PASSWORD_VALIDATION) || ''],
  };

  const validateUserForm = () => 
    !Object.keys(user)
    .reduce((isValid, prop) => isValid && user[prop].touched && user[prop].valid, true);

  const updateUser = (prop, value) => setUser({
    ...user,
    [prop]: {
      value,
      touched: true,
      valid: loginValidators[prop].reduce((isValid, validatorFn) => isValid && (typeof validatorFn(value) !== 'string'), true),
    }
  });

  const login = (ev) => {
    ev.preventDefault();

    fetch(`${BASE_URL}/login`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email:user.email.value,
        password: user.password.value,
      }),
    })
      .then(r => r.json())
      .then(({ token }) => {
        try {
          const user = decode(token);
          localStorage.setItem('token', token);
          auth.setAuthState({ user, isLoggedIn: true, isAdmin: user.isAdmin });
          props.history.push('/home');
          toast.info('Welcome on board',
            {
              position: toast.POSITION.TOP_RIGHT,
              autoClose: 2000,
            });
  
        } catch (error) {
          console.warn(error);
          toast.info('Email and password don\'t match',
            {
              position: toast.POSITION.TOP_RIGHT,
              autoClose: 2000,
            });
        }
      })
      .catch(console.warn);
    };


  return (
    <div
    className={classes.pageHeader}
    style={{
      backgroundImage: "url(" + image + ")",
      backgroundSize: "cover",
      backgroundPosition: "top-center",
      filter: 'drop-shadow(8px 8px 15px gray)',
    }}>
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <PermIdentityIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Member Sign In
        </Typography>
        <form className={classes.form}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="email"
                label="Email Address"
                name="email"
                autoFocus
                autoComplete="email"
                value={user.email.value} 
                onChange={e => updateUser('email', e.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="password"
                label="Password"
                type={showPassword ? "text" : "password"}
                id="password"
                autoComplete="current-password"
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <IconButton
                        aria-label="toggle password visibility"
                        onClick={handleClickShowPassword}
                        onMouseDown={handleMouseDownPassword}
                      >
                        {showPassword ? <Visibility /> : <VisibilityOff />}
                      </IconButton>
                    </InputAdornment>
                  ),
                }}
                value={user.password.value} 
                onChange={e => updateUser('password', e.target.value)}
              />
            </Grid>
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            required={true} 
            disabled={validateUserForm()}
            onClick={login} >
            Sign in
          </Button>
          <Grid container justify="flex-end">
            <Grid item>
              <Link style={{ color: 'black' }} href="register" variant="body2">
                Don't have an account? Sign up here
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
      <Box mt={5}>
      </Box>
    </Container>
    </div>
  );
};

export default Login;

