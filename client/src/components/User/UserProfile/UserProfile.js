import { Typography, Button} from '@material-ui/core';
import React, { useContext, useEffect, useState } from 'react';
import {BASE_URL } from '../../../common/constants.js';
import AuthContext, { getToken } from '../../../providers/authContext';
import { makeStyles } from '@material-ui/core/styles';
import { useHistory } from 'react-router-dom';
import PlaylistCard from '../../Playlist/PlaylistCard';
import AddIcon from '@material-ui/icons/Add';
import classNames from "classnames";
import GridContainer from '../../Pages/LandingPage/components/Grid/GridContainer';
import GridItem from '../../Pages/LandingPage/components/Grid/GridItem';
import Parallax from '../../Pages/LandingPage/components/Parallax/Parallax';
import styles from '../../../assets/jss/material-kit-react/views/profilePage.js';
import avatarDefault2 from '../../../assets/img/faces/avatar-default.jpg';
import UploadProfilePicture from './UploadProfilePicture.js';
import Loader from '../../Pages/Loader/Loader.js';

const useStyles = makeStyles(styles);

const UserProfile = () => {

  const classes = useStyles();
  const auth = useContext(AuthContext)
  const [userInfo, setUserInfo] = useState(null);
  const history = useHistory();
  const [userHistory, setUserHistory] = useState([]);
  const id = auth.user.id;
  const [open, setOpen] = useState(false);

  const imageClasses = classNames(
    classes.imgRaised,
    classes.imgRoundedCircle,
    classes.imgFluid
  );

  const handleClickOpen = () => {
    setOpen(true);
  };

  const fetchUser = (userId) => {
    fetch(`${BASE_URL}/users/${userId}`, {
      headers: { 'Authorization': `Bearer ${getToken()}` }
    })
    .then(r => r.json())
    .then(data => setUserInfo(data));
  };

  useEffect(() => {
    fetchUser(id);
  }, [id]);

  useEffect(() => {
    fetch(`${BASE_URL}/users/${id}/history`, {
      headers: { 'Authorization': `Bearer ${getToken()}` }
    })
    .then(r => r.json())
    .then(data => setUserHistory(data));
  }, [id]);

  if (userInfo === null) {
    return <Loader/>
  }

  if (userHistory === null) {
    return <Loader/>
  }
  
  return (
    <div>
      <Parallax
        small
        filter
        image={require("../../../assets/img/car-inside-background.jpg").default}
      />
      <div className={classNames(classes.main, classes.mainRaised)}>
        <div>
          <div className={classes.container}>
            <GridContainer justify="center">
              <UploadProfilePicture open={open} setOpen={setOpen} fetchUser={fetchUser} userId={id} setUserInfo={setUserInfo} />
              <GridItem xs={12} sm={12} md={6}>
                <div className={classes.profile}>
                  <div>
                    {userInfo.image !== null 
                    ? (<img src={`${BASE_URL}/avatars/${userInfo.image}`} 
                    alt="avatar" 
                    style={{ width: "150px", height: "150px", cursor: "pointer" }} 
                    className={imageClasses} 
                    onClick={handleClickOpen} />)
                    : (<img src={avatarDefault2} 
                      alt="avatar" 
                      style={{ width: "150px", height: "150px", cursor: "pointer" }} 
                      className={imageClasses} 
                    onClick={handleClickOpen} />) }
                  </div>
                  <div className={classes.name}>
                    <h3 className={classes.title}>{(userInfo.username).split(' ')}</h3>
                    <h6>{(userInfo.email).split(' ')}</h6>
                  </div>
                </div>
              </GridItem>
            </GridContainer>
            <GridContainer justify="center">
              <GridItem xs={12} sm={12} md={8} className={classes.navWrapper}>
                <Typography variant="h5" style={{ textAlign: "center" }} >
                  {userHistory.length !== 0 
                  ? 'My Playlists' 
                  : (<> Customize your own playlist 
                  <Button onClick={() => history.push(`/build-a-playlist`)} type="submit"> <AddIcon /> </Button> </>) }
                </Typography>
                <br></br>
                <Typography Typography gutterBottom variant="h5" component="h2">
                  {userHistory.length !== 0 
                  ? userHistory.map(p => (<PlaylistCard playlist={p} key={p.id} />))
                  : null}
                </Typography>
              </GridItem>
            </GridContainer>
          </div>
        </div>
      </div>
    </div>
  );
};

export default UserProfile;
