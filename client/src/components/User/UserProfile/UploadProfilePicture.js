import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from '@material-ui/core';
import React, { useState } from 'react';
import { BASE_URL } from '../../../common/constants';
import { getToken } from '../../../providers/authContext';
import BackupOutlinedIcon from '@material-ui/icons/BackupOutlined';
import ClearIcon from '@material-ui/icons/Clear';

const UploadProfilePicture = (props) => {
  const [file, setFile] = useState(null);

  const handleClose = () => {
    props.setOpen(false);
  };
  
  const uploadFile = (ev) => {
    ev.preventDefault();

    const fileData = new FormData();
    fileData.set('image', file);
    
    fetch(`${BASE_URL}/users/avatar`, {
      method: 'PUT',
      headers: {
        'Authorization': `Bearer ${getToken()}`
      },
      body: fileData
    })
    .then(response => response.json())
    .then(() => props.fetchUser(props.userId))
  };

  return (
    <div>
      <Dialog open={props.open} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Upload Profile Picture</DialogTitle>
        <DialogContent>
          <DialogContentText>
            <form onSubmit={uploadFile} display="inline-block">
              <input type="file" onChange={(e) => setFile(e.target.files[0]) }/>
              <DialogActions>
                <Button onClick={handleClose} variant="outlined" color="primary">
                  <ClearIcon />
                </Button>
                <Button onClick={handleClose} color="primary">
                  {file 
                  ? <Button type="submit" variant="outlined"><BackupOutlinedIcon/></Button>
                  : <Button type="submit" disabled variant="outlined"><BackupOutlinedIcon/></Button>}
                </Button>
              </DialogActions>
            </form>
          </DialogContentText>
        </DialogContent>
      </Dialog>
    </div>
  );

}

export default UploadProfilePicture;
