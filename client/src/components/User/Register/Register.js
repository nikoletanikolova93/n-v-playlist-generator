import React, { useContext, useEffect, useState } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import AuthContext from '../../../providers/authContext';
import { BASE_URL, EMAIL_VALIDATION, PASSWORD_VALIDATION, USERNAME_VALIDATION } from '../../../common/constants';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import EmailIcon from '@material-ui/icons/Email';
import { IconButton, InputAdornment } from '@material-ui/core';
import VpnKeyIcon from '@material-ui/icons/VpnKey';
import PersonPinIcon from '@material-ui/icons/PersonPin';
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import image from '../../../assets/img/login-background.jpg';


const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'inline-flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.primary.main,
  },
  form: {
    width: '100%',
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  pageHeader: {
    minHeight: "89vh",
    height: "auto",
    display: "inherit",
    position: "relative",
    margin: "0",
    padding: "0",
    border: "0",
    alignItems: "center",
    "&:before": {
      background: "rgba(0, 0, 0, 0.5)",
    },
  },
}));

export const Register = (props) => {
  const classes = useStyles();
  const history = props.history;
  const { isLoggedIn } = useContext(AuthContext);
  const [showPassword, setShowPassword] = useState(false);
  const handleClickShowPassword = () => setShowPassword(!showPassword);
  const handleMouseDownPassword = () => setShowPassword(!showPassword);

  useEffect(() => {
    if (isLoggedIn) {
      history.push('/home');
    }

  }, [history, isLoggedIn]);

  const [user, setUser] = useState({
    username: {
      value: '',
      touched: false,
      valid: false,
    },
    email: {
      value: '',
      touched: false,
      valid: false,
    },
    password: {
      value: '',
      touched: false,
      valid: false,
    },
  });

  const registerValidators = {
    username: [
      value => value.match(USERNAME_VALIDATION) || ''],
    email: [
      value => value.match(EMAIL_VALIDATION) || ''],
    password: [
      value => value.match(PASSWORD_VALIDATION) || ''],
  };

  const validateUserForm = () => 
    !Object.keys(user)
    .reduce((isValid, prop) => isValid && user[prop].touched && user[prop].valid, true);


  const updateUser = (prop, value) => setUser({
    ...user,
    [prop]: {
      value,
      touched: true,
      valid: registerValidators[prop].reduce((isValid, validatorFn) => isValid && (typeof validatorFn(value) !== 'string'), true),
    }
  });

  const register = (ev) => {
    ev.preventDefault();

    fetch(`${BASE_URL}/users`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        username: user.username.value,
        email:user.email.value,
        password: user.password.value,
      }),
    })
      .then(r => r.json())
      .then(r => {
        if (r.error) {
          toast.info('User with this email already exists!', 
            {
              position: toast.POSITION.TOP_RIGHT,
              autoClose: 2000,
            });

        } else {
          history.push('/login');
          toast.info('You have successfully registered!', 
            {
              position: toast.POSITION.TOP_RIGHT,
              autoClose: 2000,
            });
        }

      })
      .catch(console.warn);
    };


  return (
    <div
    className={classes.pageHeader}
    style={{
      backgroundImage: "url(" + image + ")",
      backgroundSize: "cover",
      backgroundPosition: "top-center",
      filter: 'drop-shadow(8px 8px 15px gray)',
    }}>

    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <PersonAddIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Create Your Account
        </Typography>
        <form className={classes.form}>
          <Grid container spacing={2}>
            <Grid item xs={12} >
              <TextField
                variant="outlined"
                required
                fullWidth
                id="username"
                label="Username"
                autoComplete="username"
                name="username"
                autoFocus
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <PersonPinIcon />
                    </InputAdornment>
                  ),
                }}
                value={user.username.value} 
                onChange={e => updateUser('username', e.target.value)}
                />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="email"
                label="Email Address"
                name="email"
                autoComplete="email"
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <EmailIcon />
                    </InputAdornment>
                  ),
                }}
                value={user.email.value} 
                onChange={e => updateUser('email', e.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="password"
                label="Password"
                type={showPassword ? "text" : "password"}
                id="password"
                autoComplete="current-password"
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <VpnKeyIcon />
                    </InputAdornment>
                  ),
                  endAdornment: (
                    <InputAdornment position="end">
                      <IconButton
                        aria-label="toggle password visibility"
                        onClick={handleClickShowPassword}
                        onMouseDown={handleMouseDownPassword}
                        >
                        {showPassword ? <Visibility /> : <VisibilityOff />}
                      </IconButton>
                    </InputAdornment>
                  ),
                }}
                inputProps={ {maxLength: 15 } }
                helperText={`${user.password.value.length}/${15}`}
                value={user.password.value} 
                onChange={e => updateUser('password', e.target.value)}
              />
            </Grid>
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            required={true} 
            disabled={validateUserForm()}
            onClick={register} >
            Sign up
          </Button>
          <Grid container justify="flex-end">
            <Grid item>
              <Link style={{ color: 'black' }} href="login" variant="body2">
                Already have an account? Sign in here
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
      <Box mt={5}>
      </Box>
    </Container>
  </div>
  );
};

export default Register;
