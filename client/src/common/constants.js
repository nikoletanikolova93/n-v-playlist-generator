export const BASE_URL = 'http://localhost:5555';

//regex validation - username should be between 3 and 45 symbols and could contain letters, numbers and _
export const USERNAME_VALIDATION = /^[a-zA-z0-9]{3,45}$/;

// regex validation - email should be in the format: example@example.com
export const EMAIL_VALIDATION = /^([\w.-]+)@([\w-]+)((\.(\w){2,3})+)$/;

// regex validation - password should be between 8 and 15 symbols and must have at least one lowercase or uppercase letter and a number
export const PASSWORD_VALIDATION = /(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{8,15})$/;

export const displaySeconds = (seconds) => {
  const format = val => `0${Math.floor(val)}`.slice(-2);
  const hours = seconds / 3600;
  const minutes = (seconds % 3600) / 60;

  return [hours, minutes, seconds % 60].map(format).join(':')
}
export const getRandomElements = (arr, num) => arr.sort(() => Math.random() - Math.random()).slice(0, num);

export const PIXABAY_API_KEY = '21680115-10d2e99c6072eca7f8b4158d6';

export const BING_API_KEY = 'AhorA7P5TXcH1akUzoS0KTN99s-qq16NWpjEIJG-ypzu1T3kH8NM7La3CnvYBJfw';
